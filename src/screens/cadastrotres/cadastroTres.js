import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Button,
  TextInput,
  Picker,
  Platform,
  KeyboardAvoidingView,
  ActivityIndicator
} from "react-native";
import ModalSelector from 'react-native-modal-selector'

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons, Entypo } from "@expo/vector-icons";
import StepIndicator from "react-native-step-indicator";
import { ScrollView } from "react-native-gesture-handler";
import { StatusBar } from "expo-status-bar";
import { LinearGradient } from "expo-linear-gradient";
import { useCadastro } from "../../context/cadastro";
import api from "../../utils/api";
import AsyncStorage from '@react-native-async-storage/async-storage';
import TopRow from "../../Components/topRow/topRow";

export default function cadastroTresScreen({ route, navigation }) {
  const {
    nomePaciente,
    setNomePaciente,
    cep,
    setCep,
    endereco,
    setEndereco,
    complemento,
    setComplemento,
    numero,
    setNumero,
    bairro,
    setBairro,
    cidade,
    setCidade,
    estado,
    setEstado,
    telefone,
    setTelefone,
    dataNascimento,
    setDataNasciemnto,
    peso,
    setPeso,
    altura,
    setAltura,
    dum,
    setDum,
    ig,
    setIg,
    sisDirUm,
    setSisDirUm,
    sisEsqUm,
    setSisEsqUm,
    sisDirDois,
    setSisDirDois,
    sisEsqDois,
    setSisEsqDois,
    diaDirUm,
    setDiaDirUm,
    diaEsqUm,
    setDiaEsqUm,
    diaDirDois,
    setDiaDirDois,
    diaEsqDois,
    setDiaEsqDois,
    hipertenso,
    setHipertenso,
    eclampPrevia,
    setEclampPrevia,
    eclampFamilia,
    setEclampFamilia,
    nulipara,
    setNulipara,
    ibge
  } = useCadastro();

  const customStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 1,
    stepStrokeCurrentColor: "#0770b0",
    stepStrokeWidth: 1,
    stepStrokeFinishedColor: "#0770b0",
    stepStrokeUnFinishedColor: "#979797",
    separatorFinishedColor: "#0770b0",
    separatorUnFinishedColor: "#0770b0",
    stepIndicatorFinishedColor: "#0770b0",
    stepIndicatorUnFinishedColor: "#979797",
    stepIndicatorCurrentColor: "#0770b0",
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: "#FFF",
    stepIndicatorLabelFinishedColor: "#FFF",
    stepIndicatorLabelUnFinishedColor: "#FFF",
    labelColor: "#FFF",
    labelSize: 20,
    currentStepLabelColor: "#FFF",
  };
  const [user, setUser] = useState();
  const [tentou, setTentou] = useState(false);
  const [loading, setLoading]= useState(false)
  const { setRefresherHome, refresherHome } = useCadastro();

  useEffect(() => {
    async function getUser() {
      const u = await AsyncStorage.getItem("@preapp:user");
      setUser(JSON.parse(u));
    }
    getUser();
  }, []);

  function createPatient() {
    console.log(
      'nome', nomePaciente,
      'cep', cep,
      'address', endereco,
      'number', numero,
      'complement', complemento,
      'district', bairro,
      'city', cidade,
      'uf', estado,
      'phone', telefone,
      'data', dataNascimento,
      'peso', peso,
      'altura', altura,
      'dum', dum,
      'sistolic_left_one:', sisEsqUm,
      'sistolic_right_one:', sisDirUm,
      'sistolic_left_two:', sisEsqDois,
      'sistolic_right_two:', sisDirDois,
      'diastolic_left_one:', diaEsqUm,
      'diastolic_right_one:', diaDirUm,
      'diastolic_left_two:', diaEsqDois,
      'diastolic_right_two:', diaDirDois,
      'have_hypertension:', hipertenso,
      'historic_eclampsy_previous:', eclampPrevia,
      'historic_family_eclampsy:', eclampFamilia,
      'nulipara:', nulipara,
      'born_date:', dataNascimento,
      'weight:', peso,
      'height:', altura,
      'dum:', dum,

    )
    api
      .post("/patients/", {
        name: nomePaciente,
        cep: cep,
        ibge: ibge,
        address: endereco,
        number: numero,
        complement: complemento,
        district: bairro,
        city: cidade,
        uf: estado,
        phone: telefone,
        born_date: dataNascimento,
      })
      .then((res) => {
        let idUser = res.data.id;
        if (res.data.id != null) {
          api
            .post(`/patients/${res.data.id}/exams`, {
              weight: peso,
              height: altura,
              ig: ig,
              sistolic_left_one: sisEsqUm,
              sistolic_right_one: sisDirUm,
              sistolic_left_two: sisEsqDois,
              sistolic_right_two: sisDirDois,
              diastolic_left_one: diaEsqUm,
              diastolic_right_one: diaDirUm,
              diastolic_left_two: diaEsqDois,
              diastolic_right_two: diaDirDois,
              have_hypertension: hipertenso,
              historic_eclampsy_previous: eclampPrevia,
              historic_family_eclampsy: eclampFamilia,
              nulipara: nulipara,
            })
            .then((res) => {
              console.log(res.data);
              if (res?.data != null) {
                setDataNasciemnto('')
                setCep('')
                setEndereco('')
                setNumero('')
                setComplemento('')
                setBairro('')
                setCidade('')
                setEstado('')
                setTelefone('')
                setDataNasciemnto('')
                setPeso('')
                setAltura('')
                setDum('')
                setIg('')
                setSisEsqUm('')
                setSisDirUm('')
                setSisEsqDois('')
                setSisDirDois('')
                setDiaEsqUm('')
                setDiaDirUm('')
                setDiaEsqDois('')
                setDiaDirDois('')
                setHipertenso(false)
                setEclampFamilia(false)
                setNulipara(false)
                setEclampPrevia(false)
                navigation.navigate("CadastroCompleto", { id: idUser, idE: res.data.id });
              }
              setLoading(false)
            })
            .catch((e) => {
              setLoading(false)
              console.log("ERRO NO EXAME!", e);
              navigation.navigate("CadastroFalha");
            });
        }
      })
      .catch((e) => {
        setLoading(false)
        console.log("ERRO NO PACIENTE!", e);
        navigation.navigate("CadastroFalha");
      });
  }

  function testInputs() {
    setLoading(true)
    setTentou(true);
    if (
      (sisEsqUm &&
        sisDirUm &&
        sisEsqDois &&
        sisDirDois &&
        diaEsqUm &&
        diaDirUm &&
        diaEsqDois &&
        diaDirDois
      ) != ""
    ) {
      setTentou(false);
      createPatient()
    }
    else{
      setLoading(false);
    }
  }
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <View style={styles.container}>
        <StatusBar backgroundColor="red" style="light" />
        <LinearGradient
          colors={["#076FAE", "#043857"]}
          start={[0.0, 0.0]}
          end={[0.3, 0.3]}
          style={styles.container}
        >
          <TopRow />
          <ScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            style={{ width: "100%" }}
          >
           
            <View style={styles.homeBody}>
              <Text style={styles.titleCenter}>CADASTRO</Text>
              <View style={{ width: "80%" }}>
                <StepIndicator
                  stepCount="3"
                  currentPosition="3"
                  customStyles={customStyles}
                />
              </View>
              
              <Text style={styles.title}>Dados Obstétricos</Text>
              
              <Text
                style={{
                  color: "#646262",
                  fontSize: 16,
                  marginTop: "5%",
                  alignSelf: "flex-start",
                  marginLeft: "2.5%",
                }}
              >
                MEDIDA 1
              </Text>
              <Text style={styles.titleWarning}>
                Inclua as medidas dos braços direito e esquerdo
              </Text>
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Sistólico Esq.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Esquerdo"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={sisEsqUm}
                    onChangeText={setSisEsqUm}
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Diastólico Esq.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Esquerdo"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={diaEsqUm}
                    onChangeText={setDiaEsqUm}
                  ></TextInput>
                </View>
              </View>
              {tentou && (sisEsqUm && diaEsqUm) == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campos obrigatórios.
                </Text>
              ) : null}
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Sistólico Dir.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Direito"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={sisDirUm}
                    onChangeText={setSisDirUm}
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Diastólico Dir.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Direito"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={diaDirUm}
                    onChangeText={setDiaDirUm}
                  ></TextInput>
                </View>
              </View>
              {tentou && (sisDirUm && diaDirUm) == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campos obrigatórios.
                </Text>
              ) : null}
              <Text
                style={{
                  color: "#646262",
                  fontSize: 16,
                  marginTop: "7.5%",
                  alignSelf: "flex-start",
                  marginLeft: "2.5%",
                }}
              >
                MEDIDA 2
              </Text>
              <Text style={styles.titleWarning}>
                A medida 2 deve ser realizada 5 minutos após a medida 1
              </Text>
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Sistólico Esq.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Esquerdo"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={sisEsqDois}
                    onChangeText={setSisEsqDois}
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Diastólico Esq.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Esquerdo"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={diaEsqDois}
                    onChangeText={setDiaEsqDois}
                  ></TextInput>
                </View>
              </View>
              {tentou && (sisEsqDois && diaEsqDois) == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campos obrigatórios.
                </Text>
              ) : null}
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Sistólico Dir.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Direito"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={sisDirDois}
                    onChangeText={setSisDirDois}
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Diastólico Dir.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Direito"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={diaDirDois}
                    onChangeText={setDiaDirDois}
                  ></TextInput>
                </View>
              </View>
              {tentou && (sisDirDois && diaDirDois) == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campos obrigatórios.
                </Text>
              ) : null}
              <View style={styles.input100}>
                <Text style={styles.inputTitle}> Hipertenso</Text>
                <ModalSelector
                  data={[
                    { key: 0, label: 'Não', value: false },
                    { key: 1, label: 'Sim', value: true }
                  ]}
                  initValue={hipertenso ? 'Sim' : 'Não'}
                  style={{
                    borderWidth: 0,
                  }}
                  selectStyle={{
                    borderWidth: 0,
                  }}
                  selectTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  initValueTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  sectionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  optionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  onChange={(option) => { setHipertenso(option.value) }} />

                {/* <Picker
                  style={styles.pickerView}
                  itemStyle={styles.pickerItem}
                  selectedValue={hipertenso}
                  onValueChange={(itemValue, itemIndex) => {
                    setHipertenso(itemValue);
                  }}
                >
                  <Picker.Item label="Não" value={false} />
                  <Picker.Item label="Sim" value={true} />
                </Picker> */}
              </View>
              <View style={styles.input100}>
                <Text style={styles.inputTitle}> Nulípara</Text>
                <ModalSelector
                  data={[
                    { key: 0, label: 'Não', value: false },
                    { key: 1, label: 'Sim', value: true }
                  ]}
                  initValue={nulipara ? 'Sim' : 'Não'}
                  style={{
                    borderWidth: 0,
                  }}
                  selectStyle={{
                    borderWidth: 0,
                  }}
                  selectTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  initValueTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  sectionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  optionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  onChange={(option) => { setNulipara(option.value) }} />
                {/* <Picker
                  selectedValue={nulipara}
                  style={styles.pickerView}
                  itemStyle={styles.pickerItem}
                  onValueChange={(itemValue, itemIndex) => {
                    setNulipara(itemValue);
                  }}
                >
                  <Picker.Item label="Não" value={false} />
                  <Picker.Item label="Sim" value={true} />
                </Picker> */}
              </View>
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>
                  {" "}
                  Histórico prévio de eclâmpsia
                </Text>
                <ModalSelector
                  data={[
                    { key: 0, label: 'Não', value: false },
                    { key: 1, label: 'Sim', value: true }
                  ]}
                  initValue={eclampPrevia ? 'Sim' : 'Não'}
                  style={{
                    borderWidth: 0,
                  }}
                  selectStyle={{
                    borderWidth: 0,
                  }}
                  selectTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  initValueTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  sectionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  optionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  onChange={(option) => { setEclampPrevia(option.value) }} />
                {/* <Picker
                  selectedValue={eclampPrevia}
                  style={styles.pickerView}
                  itemStyle={styles.pickerItem}
                  onValueChange={(itemValue, itemIndex) => {
                    setEclampPrevia(itemValue);
                  }}
                >
                  <Picker.Item label="Não" value={false} />
                  <Picker.Item label="Sim" value={true} />
                </Picker> */}
              </View>
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>
                  {" "}
                  Histórico familiar de eclâmpsia
                </Text>
                <ModalSelector
                  data={[
                    { key: 0, label: 'Não', value: false },
                    { key: 1, label: 'Sim', value: true }
                  ]}
                  initValue={eclampFamilia ? 'Sim' : 'Não'}
                  style={{
                    borderWidth: 0,
                  }}
                  selectStyle={{
                    borderWidth: 0,
                  }}
                  selectTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  initValueTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  sectionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  optionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  onChange={(option) => { setEclampFamilia(option.value) }} />
                {/* <Picker
                  selectedValue={eclampFamilia}
                  style={styles.pickerView}
                  itemStyle={styles.pickerItem}
                  onValueChange={(itemValue, itemIndex) => {
                    setEclampFamilia(itemValue);
                  }}
                >
                  <Picker.Item label="Não" value={false} />
                  <Picker.Item label="Sim" value={true} />
                </Picker> */}
              </View>
              {loading? 
                //  <View style={{width:'100%', height:'70%', justifyContent:'center', alignItems:'center'}}>
                <>
                <ActivityIndicator
                  size="large"
                  color="#0B4699"
                  animating={true}
                />
                <Text
                  style={{
                    fontSize: 20,
                    color: "#0B4699",
                    fontWeight: "bold",
                    marginTop: 20,
                  }}
                >
                  Salvando seus dados
                </Text>
                </>
              //  </View>  
              :
              (<TouchableOpacity
                style={styles.nextButton}
                onPress={() => {
                  testInputs()
                  setRefresherHome(!refresherHome)
                }}
              >
                <LinearGradient
                  colors={["#076FAE", "#0B4699"]}
                  start={[0.2, 0.0]}
                  end={[0.6, 1]}
                  style={styles.buttonContainer}
                >
                  <Text style={styles.buttonText}>Concluir cadastro</Text>
                </LinearGradient>
              </TouchableOpacity>)
              }
            </View>
          </ScrollView>
        </LinearGradient>
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    alignItems: "center",
  },
  topRow: {
    flexDirection: "row",
    height: "20%",
    width: "100%",
    paddingHorizontal: "5%",
    alignItems: "center",
  },
  profilePicture: {
    flex: 0.2,
    borderRadius: 100,
    height: "40%",
    resizeMode: "contain",
  },
  greetings: {
    flex: 0.7,
    color: "#FFF",
    fontSize: 25,
  },
  menuIcon: {
    flex: 0.1,
  },
  homeBody: {
    height: "100%",
    backgroundColor: "#FFF",
    width: "100%",
    borderTopLeftRadius: 55,
    alignItems: "center",
    paddingTop: "10%",
    paddingHorizontal: "10%",
    paddingBottom: "15%",
  },
  titleCenter: {
    color: "#979797",
    fontSize: 20,
    alignSelf: "center",
    marginBottom: "5%",
  },
  title: {
    color: "#979797",
    fontSize: 20,
    marginTop: "10%",
    alignSelf: "flex-start",
  },
  titleWarning: {
    color: "darkred",
    fontSize: 14,
    alignSelf: "flex-start",
    marginLeft: "2.5%",
  },
  input100: {
    width: "95%",
    marginTop: "5%",
    borderBottomWidth: 2,
    borderBottomColor: "#076FAE",
    paddingBottom: "2%",
  },
  inputTitle: {
    marginBottom: "2%",
    color: "#646262",
    fontSize: 16,
  },
  input50: {
    flex: 0.45,
    marginTop: "5%",
    borderBottomWidth: 1,
    borderBottomColor: "#076FAE",
    paddingBottom: "2%",
  },
  nextButton: {
    width: "80%",
    height: 50,
    marginTop: "7%",
  },
  buttonContainer: {
    borderRadius: 25,
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 20,
  },
  pickerView: {
    width: "100%",
    height: 20,
    color: "grey",
  },
  pickerItem: {
    height: Platform.OS === 'ios' ? 35 : 20,
    fontSize: 14,
  },
});
