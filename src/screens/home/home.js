import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Button,
  TextInput,
  Picker,
  TouchableOpacityBase,
  ActivityIndicator
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { useNavigation } from '@react-navigation/native';
import { Ionicons, Entypo, AntDesign } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import ActionButton from "react-native-action-button";
import PacienteCard from "../../Components/pacienteCard/pacienteCard";
import Text from "../../Components/TextRoboto";
import api from "../../utils/api";
import AsyncStorage from '@react-native-async-storage/async-storage';
import TopRow from "../../Components/topRow/topRow";
import AppMainView from "../../Components/scrollView/scrollView";
import { useCadastro } from "../../context/cadastro";

export default function homeScreen({ navigation }) {
  const [listaPacientes, setListaPacientes] = useState([]);
  const [buscaNome, setBuscaNome] = useState("");
  const [listaComExames, setListaComExames] = useState([]);
  const [user, setUser] = useState();
  const [refresh, setRefresh] = useState(false);
  const [loading, setLoading] = useState(false);
  const {refresherHome} = useCadastro();
  function refreshFunction() {
    async () =>setRefresh(true);
    getPatients();
    setRefresh(false);
  }

  useEffect(() => {
    async function getUser() {
      const u = await AsyncStorage.getItem("@preapp:user");
      setUser(JSON.parse(u));
    }
    getUser();
  }, []);
  
  useEffect(() => {
    getPatients();
    console.log(refresherHome)
  },[refresherHome]);

  function getPatients() {
  return (
    setLoading(true),
    api.get("/patients/").then((res) => {
      setListaPacientes(res.data);
      setLoading(false);
    }))
  }

  function getExames() {
    var ls = [];

    listaPacientes.map((paciente) => {
      api.get(`/patients/${paciente.id}/exams`).then((res) => {
        if(res.data.exams == null){
        }
        var idUltimoExame = res.data[res.data.length - 1].id;

        api
          .get(`/patients/${paciente.id}/exams/${idUltimoExame}`)
          .then((res) => {
            var pc = { ...paciente, exame: res.data };
            ls.push(pc);

            if (ls.length == listaPacientes.length) {
              setListaComExames(ls);
              setLoading(false)
            }
          });
      });
    });
  }

  // function getPacientes() {
  //   api.get("/patients").then((res) => {
  //     setListaPacientes(res.data);
  //   });
  // }

  // useEffect(() => {
  //   // getExames();
  //   getPacientes();
  // }, []);

  function getPatientesByName(nome) {
    setLoading(true)
    if (nome != "") {
      let lista = [];
      api
        .get(`/patients/?q=${nome}`)
        .then((res) => {
          setListaPacientes(res.data);
          setLoading(false)
        })
        .catch((e) => {
          console.log(e);
        });
    } else {
      getPatients();
    }
  }
  return (
    <>
      <View style={styles.container}>
        <StatusBar backgroundColor="#043857" style="light" />
        <LinearGradient
          colors={["#076FAE", "#043857"]}
          start={[0.0, 0.0]}
          end={[0.3, 0.3]}
          style={styles.container}
        >
          <TopRow />
          <AppMainView
            refreshFunc={() => refreshFunction()}
            refresh={refresh}
            content={
              <>
                <View  style={styles.inputContainer} >
                  <View  style={styles.textInputView }>
                    <TextInput
                      style={{ width: "100%" ,height:"100%"}}
                      placeholder="Buscar Paciente"
                      placeholderTextColor="#646262"
                      onChangeText={(value) => getPatientesByName(value)}
                    ></TextInput>
                  </View>
                  <View style={{ width: 10, height:40 }}></View>
                  <TouchableOpacity
                    style={styles.searchIconContainer}
                    onPress={() => {
                      getPatientesByName(buscaNome);
                    }}
                  >
                    <LinearGradient
                      colors={["#076FAE", "#043857"]}
                      start={[0.0, 0.0]}
                      end={[1.0, 0.4]}
                      style={styles.searchIconGradient}
                    >
                      <Ionicons name="md-search" size={24} color="white" />
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
                <View>
                  <Text style={styles.title}>PACIENTES CADASTRADAS</Text>
                  {loading ? (
                    <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                      <ActivityIndicator
                        size="large"
                        color="#0B4699"
                        animating={true}
                      />
                      <Text
                        style={{
                          fontSize: 20,
                          color: "#0B4699",
                          fontWeight: "bold",
                        }}
                      >
                        Carregando Lista de Pacientes
                    </Text>
                    </View>
                  ) : (
                    <View >
                      {listaPacientes.map((paciente) => {
                        return paciente ? (
                          <PacienteCard paciente={paciente} key={paciente.id} />
                        ) : (
                          <Text>paciente nulo</Text>
                        );
                      })}
                    </View>
                  )}
                </View>
              </>
            }
          />
        </LinearGradient>
      </View>
      <ActionButton
        buttonColor="#076FAE"
        onPress={() => {
          navigation.navigate("CadastroUm")
        }}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  inputContainer: {
    height: 40,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: "20%",
    marginBottom: 15
  },
  textInputView: {
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "#D6D6D6",
    justifyContent: "center",
    paddingLeft: "5%",
    height: 40,
    width: "90%",
  },
  searchIconContainer: {
    height: 40,
    width: 40,
    borderRadius: 20,
  },
  searchIconGradient: {
    borderRadius: 20,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    width: 40,
  },
  title: {
    color: "#979797",
    fontSize: 16,
    alignSelf: "flex-start",
    marginTop: 15
  },
  containerButton: {
    alignItems: "center",
    position: "absolute",
    bottom: 80,
    right: 60,
  },
  addButton: {
    backgroundColor: "#ffa500",
    position: "absolute",
    height: 60,
    width: 60,
    borderRadius: 60 / 2,
    alignItems: "center",
    justifyContent: "center",
    shadowRadius: 10,
    shadowColor: "#00213b",
    shadowOpacity: 0.3,
    shadowOffset: {
      height: 10,
    },
    margin: 0,
    padding: 0,
    color: "white",
    fontSize: 50,
  },
});
