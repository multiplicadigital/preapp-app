import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Button,
  TextInput,
  Picker,
  FlatList,
   ActivityIndicator
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons, Entypo, Feather } from "@expo/vector-icons";
import StepIndicator from "react-native-step-indicator";
import { ScrollView } from "react-native-gesture-handler";
import { StatusBar } from "expo-status-bar";
import { LinearGradient } from "expo-linear-gradient";
import Animated from "react-native-reanimated";
import BottomSheet from "reanimated-bottom-sheet";
import api from "../../utils/api";
import iconBaixoRisco from "../../../assets/iconGreen.png";
import iconAltoRisco from "../../../assets/iconRed.png";
import AsyncStorage from '@react-native-async-storage/async-storage';
import TopRow from "../../Components/topRow/topRow";
import { differenceInWeeks, differenceInYears } from "date-fns";

export default function classificacaoScreen({ route, navigation }) {
  const [nome, setNome] = useState("");
  const [risco, setRisco] = useState("");
  const [ig, setIg] = useState("");
  const [idade, setIdade] = useState("");
  const [altura, setAltura] = useState("");
  const [peso, setPeso] = useState("");
  const [riscoQtd, setRiscoQtd] = useState("");
  const [sisEsq, setSisEsq] = useState("");
  const [diaEsq, setDiaEsq] = useState("");
  const [pam, setPam] = useState("");
  const [imc, setImc] = useState("");
  const [color, setColor] = useState("");
  const [recomend, setRecomend] = useState("");
  const { id } = route.params;
  const { pacient } = route.params;
  const [temGrau, setTemGrau] = useState("");
  const [user, setUser] = useState();
  const [dataNascimento, setDataNascimento] = useState("")
  const [dum, setDum] = useState("")
  const [loading, setLoading]= useState(false)

  const {idE} = route.params;
  
  useEffect(() => {
    async function getUser() {
      const u = await AsyncStorage.getItem("@preapp:user");
      setUser(JSON.parse(u));
    }
    getUser();
  }, []);

  useEffect(() => {
    getClassification(id);
  }, [id,idE, pacient]);

  function getClassification(id) {
    setLoading(true)
              setIg(pacient?.exams?.ig)
              setDataNascimento(pacient?.born_date)
              let altura = parseFloat(pacient?.exams?.height);
              let peso = parseFloat(pacient?.exams?.weight);
              let IMC = peso / (altura * altura);
              setImc(IMC.toFixed(2));

              let sis = parseFloat(pacient?.exams?.sistolic_left_two);
              let dia = parseFloat(pacient?.exams?.diastolic_left_two);
              let PAM = (sis + 2 * dia) / 3;
              setPam(PAM.toFixed(2));
              setRecomend(pacient?.exams?.result?.recomendations);
              if (pacient?.exams?.result?.type === "low") {
                setRisco("BAIXO");
                setColor("green");
              } else {
                setRisco("ALTO");
                setColor("red");
              }
              setLoading(false)
  }
  function calcAge() {
    var result = differenceInYears(
      new Date(),
      new Date(dataNascimento)
    );
    return result;
  }

  const renderContent = () => (
    <View
      style={{
        display: "flex",
        paddingVertical: "10%",
        paddingHorizontal: "7.5%",
        marginTop: "50%",
        alignSelf: "center",
        width: "100%",
        height:"100%",
        backgroundColor: "#f1f1f1",
      }}
    >
      <Text
        style={{
          textAlign: "center",
          fontWeight: "bold",
          color: "#808080",
          marginBottom: "7.5%",
          fontSize:20
        }}
      >
          ____________
      </Text>
      <Text style={{ color: "#979797", fontSize: 20, marginBottom: "5%", marginTop:"4%" }}>
        RECOMENDAÇÕES
      </Text>
      {!recomend ? (<Text style={{ fontWeight: "bold", fontSize: 17, textAlign: "justify" }}>
        Não existem evidências suficientes de recomendações específicas para
        gestantes com baixo risco para pré-eclâmpsia. Porém as gestantes podem
        se beneficiar com:
      </Text>) : null}
      <View
        style={{
          flexDirection: "row",
          marginVertical: "5%",
          justifyContent: "space-between",
        }}
      >
        <Text
          style={{
            color: "#979797",
            fontWeight: "bold",
            fontSize: 20,
            flex: temGrau == false ? 1.0 : 0.7,
            textAlign: "center",
          }}
        >
          Condutas
        </Text>
        {temGrau == false ? null : (
          <Text
            style={{
              color: "#979797",
              fontWeight: "bold",
              fontSize: 20,
              flex: 0.3,
              textAlign: "center",
            }}
          >
            Grau de evidência
          </Text>
        )}
      </View>
      <View>
        <FlatList
          contentContainerStyle={{ justifyContent: "center", flex:-1, paddingBottom:0}}
          data={recomend}
          renderItem={renderItem}
          key={Math.random()}
        />
      </View>
    </View>
  );

  const sheetRef = React.useRef(null);

  const Item = ({ conduta, grau }) => (
    <View
      style={{
        flexDirection: "row",
        borderBottomWidth: 1,
        borderBottomColor: "grey",
        alignItems: "center",
        marginBottom: "2%",
        justifyContent: "center",
      }}
    >
      <Text
        style={{
          flex: grau == "" ? 1 : 0.65,
          textAlign: "justify",
          marginBottom: "5%",
        }}
      >
        {conduta}
      </Text>
      <Text style={{ flex: grau == "" ? 1 : 0.05 }}></Text>
      {grau != "" ? (
        <Text
          style={{
            flex: grau == "" ? 0 : 0.3,
            fontWeight: "bold",
            textAlign: "center",
            marginBottom: "5%",
          }}
        >
          {grau}
          {setTemGrau(true)}
        </Text>
      ) : (
        setTemGrau(false)
      )}
    </View>
  );

  const renderItem = ({ item }) => (
    <Item
      conduta={item.recomendation}
      grau={item.priority == null ? "Baixo" : item.priority}
    />
  );
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#043857" style="light" />
      <LinearGradient
        colors={["#076FAE", "#043857"]}
        start={[0.0, 0.0]}
        end={[0.3, 0.3]}
        style={styles.container}
      >
        <TopRow/>
        <ScrollView
          contentContainerStyle={{ flexGrow:1, height:'120%'}}
          style={{ width: "100%"}}
        >
          <View style={styles.homeBody}>
            <Text style={styles.titleCenter}>CLASSIFICAÇÃO DE RISCO</Text>
            <Text style={styles.name}>{nome}</Text>
            {loading? 
           <View style={{width:'100%', height:'70%', justifyContent:'center', alignItems:'center'}}>
           <ActivityIndicator
             size="large"
             color="#0B4699"
             animating={true}
           />
           <Text
             style={{
               fontSize: 20,
               color: "#0B4699",
               fontWeight: "bold",
             }}
           >
             Carregando Classificação
           </Text>
         </View>  
          :
          <>
          {pacient.exams?<View style={styles.riskView}>
              <Image
                marginBottom={10}
                style={styles.riskIcon}
                source={risco == "ALTO" ? iconAltoRisco : iconBaixoRisco}
              ></Image>
              <Text style={styles.riskClass}> Risco</Text>
              <Text
                style={{
                  fontSize: 20,
                  marginTop:-5,
                  flex: 0.4,
                  color: color,
                  //   backgroundColor: "cyan",
                  textAlign: "right",
                }}
              >
                {risco}
              </Text>
            </View>:<Text></Text>}
            {pacient.exams?<View style={styles.infoView}>
              <View style={styles.info}>
                <Text style={styles.infoName}>IG:</Text>
                <Text style={styles.infoData}>{isNaN(ig)? '' : ig + ' semanas'} </Text>
              </View>
              <View style={styles.info}>
                <Text style={styles.infoName}>Idade:</Text>
                <Text style={styles.infoData}>
                  {isNaN(calcAge())?'': calcAge() + ' anos' }</Text>
              </View>
              <View style={styles.info}>
                <Text style={styles.infoName}>IMC:</Text>
                <Text style={styles.infoData}>{imc}</Text>
              </View>
              <View style={styles.info}>
                <Text style={styles.infoName}>PAM:</Text>
                <Text style={styles.infoData}>{pam}</Text>
              </View>
              <View style={styles.info}>
                <Text style={styles.infoName}>Percentual:</Text>
                <Text style={styles.infoData}>{pacient?.exams?.result?.percente}</Text>
              </View>
            </View>:<Text style={styles.link} >Não há exames cadastrados</Text>}
            </>
          }
            
            {pacient.exams?
            <TouchableOpacity
              style={styles.button}
              onPress={() => sheetRef.current.snapTo(1)}
            >
              <LinearGradient
                colors={["#076FAE", "#0B4699"]}
                start={[0.2, 0.0]}
                end={[0.6, 1]}
                style={styles.buttonContainer}
              >
                <Text style={styles.buttonText}> Ver Recomendações</Text>
              </LinearGradient>
            </TouchableOpacity>:
            <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate("Home")}
          >
            <LinearGradient
              colors={["#076FAE", "#0B4699"]}
              start={[0.2, 0.0]}
              end={[0.6, 1]}
              style={styles.buttonContainer}
            >
              <Text style={styles.buttonText}> Voltar para ao início</Text>
            </LinearGradient>
          </TouchableOpacity>
            }
            {pacient.exams?<Text
              style={styles.link}
              onPress={() => navigation.navigate("ListaExames",{id:id, pacient: pacient, change:Math.random()})}
            >
             LISTA DE EXAMES
            </Text>:<Text></Text>}
            {pacient.exams?<Text
              style={styles.link}
              onPress={() => navigation.navigate("Home")}
            >
              VOLTAR PARA O INÍCIO
            </Text>:<Text></Text>}
          </View>
        </ScrollView>
      </LinearGradient>
      <BottomSheet
        ref={sheetRef}
        snapPoints={[0, "100%"]}
        borderRadius={50}
        renderContent={renderContent}
        initialSnap={[0]}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    alignItems: "center",
  },
  topRow: {
    flexDirection: "row",
    height: "20%",
    width: "100%",
    paddingHorizontal: "5%",
    alignItems: "center",
  },
  profilePicture: {
    flex: 0.2,
    borderRadius: 100,
    height: "40%",
    resizeMode: "contain",
  },
  greetings: {
    flex: 0.7,
    color: "#FFF",
    fontSize: 25,
  },
  menuIcon: {
    flex: 0.1,
  },
  homeBody: {
    height: "100%",
    backgroundColor: "#FFF",
    width: "100%",
    borderTopLeftRadius: 55,
    paddingHorizontal: "10%",
  },
  titleCenter: {
    color: "#979797",
    fontSize: 20,
    marginTop: "15%",
  },
  name: {
    color: "black",
    fontSize: 25,
    marginTop: "5%",
  },
  riskView: {
    flexDirection: "row",
    height: "3%",
    marginTop: "2%",
    alignItems: "center",
    width: "100%",
    justifyContent: "flex-start",
    // backgroundColor: "#ffa500",
  },
  riskIcon: {
    flex: 0.1,
    resizeMode: "contain",
    height: "100%",
    width: "100%",
  },
  riskClass: {
    color: "#646262",
    flex: 0.5,
    fontSize: 17,
  },
  riskColor: {
    fontSize: 20,
    flex: 0.4,
  },
  infoView: {
    backgroundColor: "#e2f3d1",
    height: "35%",
    borderRadius: 25,
    width: "95%",
    marginTop: "10%",
    alignSelf: "center",
    padding: "10%",
    marginBottom: "5%",
    justifyContent: "space-between",
    alignItems: "flex-start",
  },
  info: {
    flexDirection: "row",
    // paddingBottom: "10%",
  },
  infoName: {
    fontSize: 20,
    color: "#646262",
    marginRight: "5%",
  },
  infoData: {
    color: "#212121",
    fontSize: 20,
  },
  button: {
    width: "80%",
    height: "7%",
    marginTop: "5%",
    alignSelf: "center",
    marginBottom: "5%",
  },
  buttonContainer: {
    borderRadius: 25,
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 17,
  },
  link: {
    color: "#0770B0",
    fontSize: 20,
    marginTop: "5%",
    textAlign: "center",
  },
  buttonContainerModal: {
    borderRadius: 25,
    width: "100%",
    height: "60%",
    alignItems: "center",
    justifyContent: "center",
  },
});
