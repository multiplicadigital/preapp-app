import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Button,
  TextInput,
  Picker,
  Platform,
  KeyboardAvoidingView,
  Modal,
  TouchableWithoutFeedback,
  Alert
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons, Entypo, FontAwesome } from "@expo/vector-icons";
import StepIndicator from "react-native-step-indicator";
import { ScrollView } from "react-native-gesture-handler";
import { StatusBar } from "expo-status-bar";
import { LinearGradient } from "expo-linear-gradient";
import { useCadastro } from "../../context/cadastro";
import api from "../../utils/api";
import AsyncStorage from '@react-native-async-storage/async-storage';
import TopRow from "../../Components/topRow/topRow";
import ModalSelector from 'react-native-modal-selector'

let index = 0;
const data = [
  { label: "Estado", key: index++, section: true },
  { label: "AC", key: index++ },
  { label: "AL", key: index++ },
  { label: "AP", key: index++ },
  { label: "AM", key: index++ },
  { label: "BA", key: index++ },
  { label: "CE", key: index++ },
  { label: "ES", key: index++ },
  { label: "GO", key: index++ },
  { label: "MA", key: index++ },
  { label: "MT", key: index++ },
  { label: "MS", key: index++ },
  { label: "MG", key: index++ },
  { label: "PA", key: index++ },
  { label: "PB", key: index++ },
  { label: "PE", key: index++ },
  { label: "PI", key: index++ },
  { label: "RJ", key: index++ },
  { label: "RN", key: index++ },
  { label: "RS", key: index++ },
  { label: "RO", key: index++ },
  { label: "RR", key: index++ },
  { label: "SC", key: index++ },
  { label: "SP", key: index++ },
  { label: "SE", key: index++ },
  { label: "TO", key: index++ },
  { label: "DF", key: index++ },
];

export default function editCadastroScreen({ route, navigation }) {
  const { id } = route.params;
  const [nomePaciente, setNomePaciente] = useState("");
  const [cep, setCep] = useState("");
  const [endereco, setEndereco] = useState("");
  const [complemento, setComplemento] = useState("");
  const [numero, setNumero] = useState("");
  const [bairro, setBairro] = useState("");
  const [cidade, setCidade] = useState("");
  const [estado, setEstado] = useState("");
  const [telefone, setTelefone] = useState("");
  const [dataNascimento, setDataNasciemnto] = useState("");
  const [peso, setPeso] = useState("");
  const [altura, setAltura] = useState("");
  const [ig, setIg] = useState("");
  const [idade, setIdade] = useState("");
  const [confirmDel, setConfirmDel] = useState(false)
  const { setRefresherHome, refresherHome } = useCadastro();
  const [user, setUser] = useState();

  // useEffect(() => {
  //   getClassification(id);
  // }, []);

  useEffect(() => {
    async function getUser() {
      const u = await AsyncStorage.getItem("@preapp:user");
      setUser(JSON.parse(u));
    }
    getUser();
  }, []);

  useEffect(() => {
    getDados(id);
  }, [id]);

  function editUser() {
    api
      .put(`/patients/${id}`, {
        name: nomePaciente,
        cep: cep,
        ibge: 1234567,
        address: endereco,
        number: numero,
        complement: complemento,
        district: bairro,
        city: cidade,
        uf: estado,
        phone: telefone,
      })
      .then((res) => {
        navigation.navigate("Home");
      })
      .catch((e) => console.log("Erro!", e));
  }

  function deleteUser() {
    api
      .delete(`/patients/${id}`)
      .then((res) => {
        Alert.alert(
          'USUÁRIO APAGADO',
          'O usuário foi apagado com sucecsso.',
          [
            { text: 'OK', onPress: () => { 
              setConfirmDel(false)
              navigation.navigate('Home')
             } }
          ],
          { cancelable: false }
        );
      })
      .catch((e) => console.log("Erro!", e));
  }


  function getDados(id) {
    api
      .get(`/patients/${id}`)
      .then((res) => {
        setNomePaciente(res.data[0].name);
        // setIdade("Teste");
        setCep(res.data[0].cep);
        setCidade(res.data[0].city);
        setComplemento(res.data[0].complement);
        setBairro(res.data[0].district);
        setEstado(res.data[0].uf);
        setTelefone(res.data[0].phone);
        setEndereco(res.data[0].address);
        setNumero(res.data[0].number);
      })
      .catch((e) => {
        console.log("Erro", e);
      });
  }

  const customStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 1,
    stepStrokeCurrentColor: "#0770b0",
    stepStrokeWidth: 1,
    stepStrokeFinishedColor: "#0770b0",
    stepStrokeUnFinishedColor: "#979797",
    separatorFinishedColor: "#0770b0",
    separatorUnFinishedColor: "#0770b0",
    stepIndicatorFinishedColor: "#0770b0",
    stepIndicatorUnFinishedColor: "#979797",
    stepIndicatorCurrentColor: "#0770b0",
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: "#FFF",
    stepIndicatorLabelFinishedColor: "#FFF",
    stepIndicatorLabelUnFinishedColor: "#FFF",
    labelColor: "#FFF",
    labelSize: 20,
    currentStepLabelColor: "#FFF",
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <Modal
        animationType="slide"
        transparent={true}
        visible={confirmDel}
        onRequestClose={() => setWrongUser(false)}
      >
        <TouchableOpacity
          style={{
            borderRadius: 25,
            height: "100%",
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(150, 30, 15, .4)",
          }}
          onPress={() => setConfirmDel(false)}
        >
          <View
            style={{
              backgroundColor: "rgba(255, 255, 255, .9)",
              width: "80%",
              height: "30%",
              borderRadius: 25,
              justifyContent: "center",
              alignItems: "center",
              paddingHorizontal: "10%",
              borderColor: 'red',
              borderWidth: 2
            }}
          >
            <FontAwesome name="warning" size={50} color="red" />
            <Text
              style={{
                fontSize: 20,
                fontWeight: "bold",
                color: "#076FAE",
                paddingVertical: "5%",
                textAlign: 'center'
              }}
            >
              {" "}
             Tem certeza que deseja deletar o usuário?
            </Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%' }}>
              <TouchableWithoutFeedback
                style={{ marginTop: "5%" }}
                onPress={() => {
                  deleteUser()
                  setRefresherHome(!refresherHome);
                }}
              >
                <Text
                  style={{ fontSize: 20, fontWeight: "bold", color: "red" }}
                >
                  Sim
              </Text>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
                style={{ marginTop: "5%" }}
                onPress={() => setConfirmDel(false)}
              >
                <Text
                  style={{ fontSize: 20, fontWeight: "bold", color: "#076FAE" }}
                >
                  Não
              </Text>
              </TouchableWithoutFeedback>
            </View>
          </View>
        </TouchableOpacity>
      </Modal>

      <View style={styles.container}>

        <StatusBar backgroundColor="#043857" style="light" />

        <LinearGradient
          colors={["#076FAE", "#043857"]}
          start={[0.0, 0.0]}
          end={[0.3, 0.3]}
          style={styles.container}
        >

          <TopRow />

          <ScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            style={{ width: "100%" }}
          >




            <View style={styles.homeBody}>

              <Text style={styles.titleCenter}> EDITAR CADASTRO</Text>
              <View style={{ width: "80%" }}></View>
              <View style={styles.input100}>

                <Text style={styles.inputTitle}>Nome da Paciente</Text>
                <TextInput
                  value={nomePaciente}
                  onChangeText={setNomePaciente}
                  style={{ paddingLeft: 10 }}
                  placeholder="Digite o nome da paciente"
                  placeholderTextColor="#707070"
                ></TextInput>
              </View>
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>CEP</Text>
                <TextInput
                  value={cep}
                  onChangeText={setCep}
                  style={{ paddingLeft: 10 }}
                  placeholder="CEP"
                  placeholderTextColor="#707070"
                  keyboardType="number-pad"
                ></TextInput>
              </View>
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>Endereço</Text>
                <TextInput
                  value={endereco}
                  onChangeText={setEndereco}
                  style={{ paddingLeft: 10 }}
                  placeholder="Endereço"
                  placeholderTextColor="#707070"
                ></TextInput>
              </View>
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Complemento</Text>
                  <TextInput
                    value={complemento}
                    onChangeText={setComplemento}
                    style={{ paddingLeft: 10 }}
                    placeholder="Complemento"
                    placeholderTextColor="#707070"
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Número</Text>
                  <TextInput
                    value={numero}
                    onChangeText={setNumero}
                    style={{ paddingLeft: 10 }}
                    placeholder="Número"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                  ></TextInput>
                </View>
              </View>
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>Bairro</Text>
                <TextInput
                  value={bairro}
                  onChangeText={setBairro}
                  style={{ paddingLeft: 10 }}
                  placeholder="Bairro"
                  placeholderTextColor="#707070"
                ></TextInput>

              </View>

              <View style={{ flexDirection: "row", width: "95%", zIndex: 5 }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Cidade</Text>
                  <TextInput
                    value={cidade}
                    onChangeText={setCidade}
                    style={{ paddingLeft: 10 }}
                    placeholder="Cidade"
                    placeholderTextColor="#707070"
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Estado</Text>
                  <ModalSelector
                    data={data}
                    initValue={estado || "Estado"}
                    style={{
                      borderWidth: 0,
                    }}
                    selectStyle={{
                      borderWidth: 0,
                    }}
                    selectTextStyle={{
                      color: '#000',
                      fontSize: 14
                    }}
                    initValueTextStyle={{
                      color: '#000',
                      fontSize: 14
                    }}
                    sectionTextStyle={{
                      color: '#000',
                      fontSize: 14
                    }}
                    optionTextStyle={{
                      color: '#000',
                      fontSize: 14
                    }}
                    onChange={(option) => { setEstado(option.label) }} />
                </View>
              </View>



              <View style={styles.input100}>
                <Text style={styles.inputTitle}>Telefone</Text>
                <TextInput
                  value={telefone}
                  onChangeText={setTelefone}
                  style={{ paddingLeft: 10, zIndex: 10 }}
                  placeholder="Telefone"
                  placeholderTextColor="#707070"
                  keyboardType="number-pad"
                ></TextInput>
              </View>
              <TouchableOpacity
                style={styles.nextButton}
                onPress={() => editUser()}
              >
                <LinearGradient
                  colors={["#076FAE", "#0B4699"]}
                  start={[0.2, 0.0]}
                  end={[0.6, 1]}
                  style={styles.buttonContainer}
                >
                  <Text style={styles.buttonText}> Salvar</Text>
                </LinearGradient>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.nextButton}
                onPress={() => {
                  setConfirmDel(true)
                  setRefresherHome(!refresherHome);
                  }}
              >
                <LinearGradient
                  colors={["red", "darkred"]}
                  start={[0.2, 0.0]}
                  end={[0.6, 1]}
                  style={styles.buttonContainer}
                >
                  <Text style={styles.buttonText}> Apagar Usuário</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </LinearGradient>
      </View>


    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    alignItems: "center",
  },
  topRow: {
    flexDirection: "row",
    height: "20%",
    width: "100%",
    paddingHorizontal: "5%",
    alignItems: "center",
  },
  profilePicture: {
    flex: 0.2,
    borderRadius: 100,
    height: "40%",
    resizeMode: "contain",
  },
  greetings: {
    flex: 0.7,
    color: "#FFF",
    fontSize: 25,
  },
  menuIcon: {
    flex: 0.1,
  },
  homeBody: {
    height: "100%",
    backgroundColor: "#FFF",
    width: "100%",
    borderTopLeftRadius: 55,
    alignItems: "center",
    paddingVertical: "10%",
    paddingHorizontal: "10%",
    zIndex: 5,
  },
  titleCenter: {
    color: "#979797",
    fontSize: 20,
    alignSelf: "center",
    marginBottom: "5%",
  },
  title: {
    color: "#979797",
    fontSize: 20,
    marginTop: "10%",
    alignSelf: "flex-start",
  },
  input100: {
    width: "95%",
    marginTop: "5%",
    borderBottomWidth: 1,
    borderBottomColor: "#076FAE",
    paddingBottom: "2%",
    zIndex: 5

  },
  inputTitle: {
    marginBottom: "2%",
    color: "#646262",
    fontSize: 16,
  },
  input50: {
    flex: 0.45,
    zIndex: 20,
    marginTop: "5%",
    borderBottomWidth: 1,
    borderBottomColor: "#076FAE",
    // backgroundColor: "#076FAE",
    paddingBottom: "2%",

  },
  nextButton: {
    width: "80%",
    height: 50,
    marginTop: "5%",
  },
  buttonContainer: {
    borderRadius: 25,
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 20,
  },
  pickerView: {
    width: "100%",
    height: 20,
    color: "grey",
    borderWidth: 0
  },
  pickerItem: {
    height: Platform.OS === 'ios' ? 35 : 20,
    fontSize: 14,
  },
});
