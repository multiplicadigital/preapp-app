import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Button,
  TextInput,
  Picker,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons, Entypo, Feather } from "@expo/vector-icons";
import StepIndicator from "react-native-step-indicator";
import { ScrollView } from "react-native-gesture-handler";
import { StatusBar } from "expo-status-bar";
import { LinearGradient } from "expo-linear-gradient";
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function cadastroFalhaScreen({ route, navigation }) {
  const [user, setUser] = useState();

  useEffect(() => {
    async function getUser() {
      const u = await AsyncStorage.getItem("@preapp:user");
      setUser(JSON.parse(u));
    }
    getUser();
  }, []);

  function redirect() {
    setTimeout(() => {
      navigation.navigate("Home");
    }, 3000);
  }
  useEffect(() => {
    redirect();
  }, []);
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#043857" style="light" />
      <LinearGradient
        colors={["#076FAE", "#043857"]}
        start={[0.0, 0.0]}
        end={[0.3, 0.3]}
        style={styles.container}
      >
        <View style={styles.topRow}>
          <Image
            style={styles.profilePicture}
            source={require("../../../assets/medico.png")}
          ></Image>
          <Text style={styles.greetings}>Olá, {user ? user.name : "Dr"}</Text>
          <Entypo style={styles.menuIcon} name="menu" size={40} color="white"  onPress={() => navigation.toggleDrawer()}/>
        </View>
        <View style={styles.homeBody}>
          <View style={styles.greenCircle}>
            <Feather name="x" size={60} color="white" />
          </View>
          <Text style={styles.title}>
            {" "}
            Cadastro não foi concluído com sucesso! Tente novamente!
          </Text>
        </View>
      </LinearGradient>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    alignItems: "center",
  },
  topRow: {
    flexDirection: "row",
    height: "20%",
    width: "100%",
    paddingHorizontal: "5%",
    alignItems: "center",
  },
  profilePicture: {
    flex: 0.2,
    borderRadius: 100,
    height: "40%",
    resizeMode: "contain",
  },
  greetings: {
    flex: 0.7,
    color: "#FFF",
    fontSize: 25,
  },
  menuIcon: {
    flex: 0.1,
  },
  homeBody: {
    height: "100%",
    backgroundColor: "#FFF",
    width: "100%",
    borderTopLeftRadius: 55,
    alignItems: "center",
    paddingHorizontal: "10%",
  },
  greenCircle: {
    width: "35%",
    height: "15.5%",
    borderRadius: 100,
    backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "45%",
  },
  title: {
    color: "#646262",
    fontSize: 25,
    marginTop: "15%",
    width: "80%",
    textAlign: "center",
  },
});
