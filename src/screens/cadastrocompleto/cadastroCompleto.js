import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
} from "react-native";
import { StatusBar } from "expo-status-bar";
import { LinearGradient } from "expo-linear-gradient";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Entypo } from "@expo/vector-icons";

export default function cadastroCompletoScreen({ route, navigation }) {
  const { id, text } = route.params;
  const { idE } = route.params;



  const [user, setUser] = useState();

  useEffect(() => {
    async function getUser() {
      const u = await AsyncStorage.getItem("@preapp:user");
      setUser(JSON.parse(u));
    }
    getUser();
  }, []);

  function redirect() {
    setTimeout(() => {
      // navigation.navigate("Classificacao", { id: id, idE: Math.random() });
      navigation.navigate("Home")
    }, 3000);
  }
  useEffect(() => {
    redirect();
  }, [id, idE]);
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#043857" style="light" />
      <LinearGradient
        colors={["#076FAE", "#043857"]}
        start={[0.0, 0.0]}
        end={[0.3, 0.3]}
        style={styles.container}
      >
        <View style={styles.topRow}>
          <Image
            style={styles.profilePicture}
            source={require("../../../assets/medico.png")}
          ></Image>
          <Text style={styles.greetings}>Olá, {user ? user.name : "Dr"}</Text>
          <Entypo style={styles.menuIcon} name="menu" size={40} color="white" onPress={() => navigation.toggleDrawer()} />
        </View>
        <View style={styles.homeBody}>
          <View style={styles.greenCircle}>
            <Image
              style={{ height: "30%", resizeMode: "contain" }}
              source={require("../../../assets/completo.png")}
            />
          </View>
          <Text style={styles.title}> {text || 'Cadastro concluído com sucesso!'}</Text>
        </View>
      </LinearGradient>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    alignItems: "center",
  },
  topRow: {
    flexDirection: "row",
    height: "20%",
    width: "100%",
    paddingHorizontal: "5%",
    alignItems: "center",
  },
  profilePicture: {
    flex: 0.2,
    borderRadius: 100,
    height: "40%",
    resizeMode: "contain",
  },
  greetings: {
    flex: 0.7,
    color: "#FFF",
    fontSize: 25,
  },
  menuIcon: {
    flex: 0.1,
  },
  homeBody: {
    height: "100%",
    backgroundColor: "#FFF",
    width: "100%",
    borderTopLeftRadius: 55,
    alignItems: "center",
    paddingHorizontal: "10%",
  },
  greenCircle: {
    width: 150,
    height: 150,
    borderRadius: 75,
    backgroundColor: "#5FA719",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "45%",
  },
  title: {
    color: "#646262",
    fontSize: 25,
    marginTop: "15%",
    width: "80%",
    textAlign: "center",
  },
});
