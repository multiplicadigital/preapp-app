import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Button,
  TextInput,
  Picker,
  FlatList,
  ActivityIndicator,
  Alert,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons, Entypo, Feather, FontAwesome } from "@expo/vector-icons";
import StepIndicator from "react-native-step-indicator";
import { ScrollView } from "react-native-gesture-handler";
import { StatusBar } from "expo-status-bar";
import { LinearGradient } from "expo-linear-gradient";
import Animated from "react-native-reanimated";
import BottomSheet from "reanimated-bottom-sheet";
import api from "../../utils/api";
import iconBaixoRisco from "../../../assets/iconGreen.png";
import iconAltoRisco from "../../../assets/iconRed.png";
import AsyncStorage from '@react-native-async-storage/async-storage';
import TopRow from "../../Components/topRow/topRow";
import { differenceInWeeks, differenceInYears } from "date-fns";
import IconGoBack from "../../Components/IconGoBack";

export default function listaExamesScreen({ route, navigation }) {
  const { id } = route.params;
  const { pacient } = route.params;
  const { change } = route.params;
  const [userId, setUserId] = useState("")
  const [exameId, setExameID] = useState("")
  const [exames, setExames] = useState([]);
  useEffect(() => {
    async function getUser() {
      const u = await AsyncStorage.getItem("@preapp:user");
      setUser(JSON.parse(u));
    }
    getUser();
  }, []);

  useEffect(() => {
    setUserId(pacient?.id)
    // getClassification(pacient?.id);
  }, [change, pacient]);

  function getClassification(id) {
    api
      .get(`/patients/${id}`)
      .then((res) => {
        api.get(`/patients/${id}/exams/`).then((res) => {
          setExames(res.data);
        });
      })
      .catch((e) => {
        console.log("Erro", e);
      });
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#043857" style="light" />
      <LinearGradient
        colors={["#076FAE", "#043857"]}
        start={[0.0, 0.0]}
        end={[0.3, 0.3]}
        style={styles.container}
      >
        <TopRow />
        <View style={styles.homeBody}>
          <ScrollView
            contentContainerStyle={{
              flexGrow: 1,
              paddingBottom: 200,
            }}
            style={{ width: "100%" }}
          >

            <Text style={styles.titleCenter}>Lista de exames</Text>
            {pacient.exams?<Text style={styles.link}>
              Clique no card do exame para abrí-lo
            </Text>:<Text style={styles.link}> Não há exames cadastrados</Text>}
           {/* {exames.map((paciente) => { */}
              {/* { return pacient ? ( */}
                {pacient?.exams?<TouchableOpacity
                  onPress={() => pacient?.exams?navigation.navigate('Exame', { userID: userId, exameID: pacient.exams.id, pacient:pacient }):Alert.alert(
                    "Erro na visualização",
                    "Não há exames cadastrados.",
                    [
                      {
                        text: "Fechar",
                        style: "cancel"
                      },
                    ]
                  )}
                  style={{
                    justifyContent: "space-around",
                    width: "100%",
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: "#0770B0",
                    marginTop: 15,
                    flexDirection: "row",
                  }}
                >
                  <View
                    style={{
                      justifyContent: "center",
                      alignItems: "center",
                      flex: 0.3,
                      backgroundColor: "#0770B0",
                    }}
                  >
                    <FontAwesome name="stethoscope" size={40} color="white" />
                  </View>
                  <View style={{ flex: 0.7, padding: 5 }}>
                    <Text style={{ color: "#979797", textAlign: "center" }}>
                      {pacient?.exams?"Exame realizado no dia "+(pacient.exams?.created_at.substr(8, 2) +
                        "/" +
                        pacient.exams?.created_at.substr(5, 2) +
                        "/" +
                        pacient.exams?.created_at.substr(0, 4)):"Sem cadastro de Exames"}
                    </Text>
                    <Text style={{ color: "#043857", flex: 0.5 }}>
                      Altura: {pacient.exams?.height?pacient.exams?.height:""}
                    </Text>
                    <Text style={{ color: "#043857", flex: 0.5 }}>
                      Peso: {pacient.exams?.weight?pacient.exams?.weight:""}
                    </Text>
                    <Text style={{ color: "#043857" }}>
                      {""}
                     IG :
                      {" " +
                        pacient?.exams?.ig?pacient.exams?.ig:""}
                    </Text>
                  </View>
                </TouchableOpacity>:<Text></Text>}
              {/* ) : null; */}
            {/* })} */}
          </ScrollView>
        </View>
      </LinearGradient>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    alignItems: "center",
  },
  topRow: {
    flexDirection: "row",
    height: "20%",
    width: "100%",
    paddingHorizontal: "5%",
    alignItems: "center",
  },
  profilePicture: {
    flex: 0.2,
    borderRadius: 100,
    height: "40%",
    resizeMode: "contain",
  },
  greetings: {
    flex: 0.7,
    color: "#FFF",
    fontSize: 25,
  },
  menuIcon: {
    flex: 0.1,
  },
  homeBody: {
    height: "100%",
    backgroundColor: "#FFF",
    width: "100%",
    borderTopLeftRadius: 55,
    paddingHorizontal: "10%",
  },
  titleCenter: {
    color: "#979797",
    fontSize: 20,
    marginTop: 20,
  },
  name: {
    color: "black",
    fontSize: 25,
    marginTop: "5%",
  },
  riskView: {
    flexDirection: "row",
    height: "3%",
    marginTop: "2%",
    alignItems: "center",
    width: "100%",
    justifyContent: "flex-start",
    // backgroundColor: "#ffa500",
  },
  riskIcon: {
    flex: 0.1,
    resizeMode: "contain",
    height: "100%",
    width: "100%",
  },
  riskClass: {
    color: "#646262",
    flex: 0.5,
    fontSize: 17,
  },
  riskColor: {
    fontSize: 20,
    flex: 0.4,
  },
  infoView: {
    backgroundColor: "#e2f3d1",
    height: "35%",
    borderRadius: 25,
    width: "95%",
    marginTop: "10%",
    alignSelf: "center",
    padding: "10%",
    marginBottom: "5%",
    justifyContent: "space-between",
    alignItems: "flex-start",
  },
  info: {
    flexDirection: "row",
    // paddingBottom: "10%",
  },
  infoName: {
    fontSize: 20,
    color: "#646262",
    marginRight: "5%",
  },
  infoData: {
    color: "#212121",
    fontSize: 20,
  },
  button: {
    width: "80%",
    height: "7%",
    marginTop: "5%",
    alignSelf: "center",
    marginBottom: "5%",
  },
  buttonContainer: {
    borderRadius: 25,
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 17,
  },
  link: {
    color: "#0770B0",
    fontSize: 15,
    marginTop: "5%",
    textAlign: "center",
  },
  buttonContainerModal: {
    borderRadius: 25,
    width: "100%",
    height: "60%",
    alignItems: "center",
    justifyContent: "center",
  },
});
