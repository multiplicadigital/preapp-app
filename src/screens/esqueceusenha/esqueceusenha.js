import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Button,
  TextInput,
  Picker,
  Alert,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import CheckBox from "@react-native-community/checkbox";
import { LinearGradient } from "expo-linear-gradient";
import api from "../../utils/api";
import * as Yup from 'yup';

export default function esqueceuSenhaScreen({navigation}) {
  const [email, setEmail] = useState("");

  const schema = Yup.object().shape({
    email: Yup.string()
      .email()
      .required()
  })

  const handleRecuperarSenha = async () => {
    try {
      if(await schema.isValid({email: email})){
        Alert.alert(
          "Token enviado",
          "Verifique seu endereço de email",
          [
            {
              text: "Fechar",
              style: "cancel"
            },
          ]
        )
        await api.post("/ForgotPassword/", { email });
        navigation.navigate("NovaSenha");
      }
      else{
        Alert.alert(
          "Email incorreto ou campo vazio",
          "Por favor entre com um email válido",
          [
            {
              text: "Fechar",
              style: "cancel"
            },
          ]
        )
      }
    } catch (err) {
      console.log(err)
    }
  };
  return (
    <View style={styles.container}>
      <Image
        style={{ resizeMode: "contain", width: "35%", height: "30%" }}
        source={require("../../../assets/logo_nome.png")}
      />
      <View
        style={{
          borderLeftColor: "#0B4699",
          borderLeftWidth: 4,
          backgroundColor: "#FFF",
          width: "80%",
          padding: "5%",
          marginBottom: "5%",
        }}
      >
        <Text style={{ color: "#707070", fontSize: 20 }}>
          Digite seu endereço de e-mail. Você receberá um token para criar uma
          nova senha.
        </Text>
      </View>
      <View style={styles.loginInput}>
        <Text style={styles.inputLabel}> E-mail</Text>
        <View style={styles.inputContainer}>
          <LinearGradient
            colors={["#076FAE", "#0B4699"]}
            start={[0.5, 0.5]}
            end={[1.0, 1.0]}
            style={styles.iconContainer}
          >
            <MaterialCommunityIcons
              name="account-question-outline"
              size={24}
              color="white"
            />
          </LinearGradient>
          <TextInput
            placeholder="E-mail"
            placeholderTextColor="#707070"
            style={styles.input}
            value={email}
            autoCapitalize="none"
            onChangeText={setEmail}
          ></TextInput>
        </View>
      </View>
      <TouchableOpacity style={styles.button} onPress={handleRecuperarSenha}>
        <LinearGradient
          colors={["#076FAE", "#0B4699"]}
          start={[0.2, 0.0]}
          end={[0.6, 1]}
          style={styles.buttonContainer}
        >
          <Text style={styles.buttonText}> Obter nova senha</Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    paddingTop: "5%",
    alignItems: "center",
    marginTop: "10%",
    backgroundColor: "#f1f1f1",
  },
  button: {
    width: "100%",
    height: "100%",
    marginTop: "5%",
    marginBottom: "5%",
    alignItems: "center",
  },
  buttonContainer: {
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
    height: "7.5%",
    width: "60%",
  },
  loginInput: {
    width: "100%",
    alignItems: "flex-start",
    paddingHorizontal: "10%",
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 17,
  },
  inputLabel: {
    color: "#707070",
    fontWeight: "100",
    fontSize: 15,
    marginVertical: "2%",
  },
  inputContainer: {
    flexDirection: "row",
    width: "100%",
    backgroundColor: "white",
    borderRadius: 5,
    height: 46,
    marginBottom: "2%",
  },
  iconContainer: {
    // flex: 0.17,
    justifyContent: "center",
    alignItems: "center",
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    width: "16%",
  },
  input: {
    overflow: "hidden",
    marginLeft: "2%",
    height: "100%",
    width: "100%",
  },
});
