import React, { useState, useEffect,Component } from "react";
import { SafeAreaFrameContext } from "react-native-safe-area-context";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { render } from "react-dom";
import { View } from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { Ionicons, Entypo } from "@expo/vector-icons";
import {
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
    Button,
    TextInput,
    Picker,
    Alert,
  } from "react-native";
import api from "../../utils/api";
import AuthRoutes from "../../routes/auth.routes";
import loginScreen from "../login/login";
import * as yup from 'yup';


export default function cadastroScreen({navigation}){
    const [nome, setNome] = useState("");
    const [email, setemail] = useState("");
    const [senha, setsenha] = useState("");
    const [senhaVis, setSenhaVis] = useState(false);
    const [validador,setValidador] = useState(false)

    const schema = yup.object().shape({
      name: yup.string().required(),
      email: yup.string().email().required(),
      password: yup.string().min(6).required()
    })
    
    const EnviarES = async () => {
        if(setNome == "" || setemail == "" || setsenha == "")
        {
            setValidador = true
        }
        else{
        try {
          if(await schema.isValid({name: nome, email: email, password: senha})){
            Alert.alert(
              "Sucesso",
              "Usuário cadastrado com sucesso",
              [
                {
                  text: "Fechar",
                  style: "cancel"
                },
              ]
            )
            navigation.goBack();
            await api.post("/Users/", {name: nome, email: email, password: senha});
            
          }
          else{
            Alert.alert(
              "Erro no preenchimento dos campos",
              "Verifique se todos os campos estão preenchidos corretamente",
              [
                {
                  text: "Fechar",
                  style: "cancel"
                },
              ]
            )
          }
        } catch (error) {
          console.log("Erro encontrado:" + error)
         }
        }}

        return (
            <View style={styles.container}>
                <View style={{
                    borderLeftColor: "#0B4699",
                    borderLeftWidth: 4,
                    backgroundColor: "#FFF",
                    width: "80%",
                    padding: "5%",
                    marginBottom: "5%",
                }} >
                    <Text style={{ color: "#707070", fontSize: 20 }}>
                        Digite seu nome, endereço de e-mail e sua senha.
                    </Text>
                </View>
                <View style={styles.loginInput}>
                <Text style={styles.inputLabel}> Nome </Text>
                    <View style={styles.inputContainer}>
                    <LinearGradient
                        colors={["#076FAE", "#0B4699"]}
                        start={[0.5, 0.5]}
                        end={[1.0, 1.0]}
                        style={styles.iconContainer}
                        >
                            <MaterialCommunityIcons
                            name="account-question-outline"
                            size={24}
                            color="white"
                            />
                        </LinearGradient>
                        <TextInput            
                        placeholder="Nome de Usuário"
                        placeholderTextColor="#707070"
                        style={styles.input}
                        value={nome}
                        onChangeText={setNome}> 
                        </TextInput>
                    </View>
                    <Text style={styles.inputLabel}> E-mail </Text>
                    <View style={styles.inputContainer}>
                        <LinearGradient
                        colors={["#076FAE", "#0B4699"]}
                        start={[0.5, 0.5]}
                        end={[1.0, 1.0]}
                        style={styles.iconContainer}
                        >
                            <MaterialCommunityIcons
                            name="email-outline"
                            size={24}
                            color="white"
                            />
                        </LinearGradient>
                        <TextInput            
                        placeholder="email@test.com.br"
                        placeholderTextColor="#707070"
                        style={styles.input}
                        value={email}
                        onChangeText={setemail}> 
                        </TextInput> 
                    </View>
                    <Text style={styles.inputLabel}> Senha </Text>
                    <View style={styles.inputContainer}>
                    <LinearGradient
                        colors={["#076FAE", "#0B4699"]}
                        start={[0.5, 0.5]}
                        end={[1.0, 1.0]}
                        style={styles.iconContainer}
                        >
                        <Image
                            style={{ width: 25, height: 25 }}
                            source={require("../../../assets/icon_lock.png")}
                        />
                        </LinearGradient>
                        <TextInput            
                        placeholder="Senha"
                        placeholderTextColor="#707070"
                        secureTextEntry={! senhaVis}
                        style={styles.input}
                        value={senha}
                        onChangeText={setsenha}> 
                        </TextInput>
                    </View>
                    <TouchableOpacity style={styles.button} onPress={EnviarES}>
                        <LinearGradient
                        colors={["#076FAE", "#0B4699"]}
                        start={[0.2, 0.0]}
                        end={[0.6, 1]}
                        style={styles.buttonContainer}
                        >
                        <Text style={styles.buttonText}> Cadastrar</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
                {setValidador == true ? (
                    <Text style={{ color: "red", fontSize: 12 }}>
                        É necessário um endereço de e-mail para fazer login.
                    </Text>
                ) : null}
            </View>
        );
}
const styles = StyleSheet.create({
    container: {
      height: "100%",
      paddingTop: "5%",
      alignItems: "center",
      marginTop: "10%",
      backgroundColor: "#f1f1f1",
    },
    button: {
      width: "100%",
      height: "100%",
      marginTop: "5%",
      marginBottom: "5%",
      alignItems: "center",
    },
    buttonContainer: {
      borderRadius: 25,
      alignItems: "center",
      justifyContent: "center",
      height: "6%",
      width: "60%",
    },
    loginInput: {
      width: "100%",
      alignItems: "flex-start",
      paddingHorizontal: "10%",
    },
    buttonText: {
      color: "#FFFFFF",
      fontSize: 17,
    },
    inputLabel: {
      color: "#707070",
      fontWeight: "100",
      fontSize: 15,
      marginVertical: "2%",
    },
    inputContainer: {
      flexDirection: "row",
      width: "100%",
      backgroundColor: "white",
      borderRadius: 5,
      height: 46,
      marginBottom: "2%",
    },
    iconContainer: {
      // flex: 0.17,
      justifyContent: "center",
      alignItems: "center",
      borderTopLeftRadius: 5,
      borderBottomLeftRadius: 5,
      borderTopRightRadius: 20,
      borderBottomRightRadius: 20,
      width: "16%",
    },
    input: {
      overflow: "hidden",
      marginLeft: "2%",
      height: "100%",
      width: "100%",
    },
  });
  

