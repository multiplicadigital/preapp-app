import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Button,
  TextInput,
  Picker,
  FlatList,
  ActivityIndicator,
  KeyboardAvoidingView,
  Modal,
  TouchableWithoutFeedback,
  Alert
} from "react-native";
import ModalSelector from 'react-native-modal-selector'
import { FontAwesome } from "@expo/vector-icons";
import { ScrollView } from "react-native-gesture-handler";
import { StatusBar } from "expo-status-bar";
import { LinearGradient } from "expo-linear-gradient";
import api from "../../utils/api";
import AsyncStorage from '@react-native-async-storage/async-storage';
import TopRow from "../../Components/topRow/topRow";
import { useCadastro } from "../../context/cadastro";

export default function exameScreen({ route, navigation }) {
  const { id } = route.params;
  const { userID } = route.params;
  const { exameID } = route.params;
  const { pacient } = route.params;
  const [nomePaciente, setNomePaciente] = useState("");
  const [cep, setCep] = useState("");
  const [endereco, setEndereco] = useState("");
  const [complemento, setComplemento] = useState("");
  const [numero, setNumero] = useState("");
  const [bairro, setBairro] = useState("");
  const [cidade, setCidade] = useState("");
  const [estado, setEstado] = useState("");
  const [telefone, setTelefone] = useState("");
  const [peso, setPeso] = useState("");
  const [altura, setAltura] = useState("");
  const [sisDirUm, setSisDirUm] = useState("");
  const [sisEsqUm, setSisEsqUm] = useState("");
  const [sisDirDois, setSisDirDois] = useState("");
  const [sisEsqDois, setSisEsqDois] = useState("");
  const [diaDirUm, setDiaDirUm] = useState("");
  const [diaEsqUm, setDiaEsqUm] = useState("");
  const [diaDirDois, setDiaDirDois] = useState("");
  const [diaEsqDois, setDiaEsqDois] = useState("");
  const [hipertenso, setHipertenso] = useState("");
  const [eclampPrevia, setEclampPrevia] = useState("");
  const [eclampFamilia, setEclampFamilia] = useState("");
  const [nulipara, setNulipara] = useState("");
  const [tentou, setTentou] = useState(false);
  const [user, setUser] = useState();
  const [confirmDel, setConfirmDel] = useState(false)
  const [ig, setIg] = useState('')
  const { setRefresherHome, refresherHome } = useCadastro();
  // useEffect(() => {
  //   getClassification(id);
  // }, []);

  useEffect(() => {
    async function getUser() {
      const u = await AsyncStorage.getItem("@preapp:user");
      setUser(JSON.parse(u));
    }
    getUser();
  }, []);

  useEffect(() => {
    getDados(userID, exameID);
  }, [userID, exameID, pacient]);

  function editUser() {
    api
      .put(`/patients/${userID}/exams/${exameID}`, {
        
        weight: peso,
        height: altura,
        ig: ig,
        sistolic_left_one: sisEsqUm,
        sistolic_right_one: sisDirUm,
        sistolic_left_two: sisEsqDois,
        sistolic_right_two: sisDirDois,
        diastolic_left_one: diaEsqUm,
        diastolic_right_one: diaDirUm,
        diastolic_left_two: diaEsqDois,
        diastolic_right_two: diaDirDois,
        have_hypertension: hipertenso,
        historic_eclampsy_previous: eclampPrevia,
        historic_family_eclampsy: eclampFamilia,
        nulipara: nulipara
        
      })
      .then((res) => {
        navigation.navigate("CadastroCompleto", { id: userID, idE: Math.random(), text: 'O exame foi editado!' });
      })
      .catch((e) => {
        navigation.navigate("CadastroFalha");
      });
    }
  

  function convert(str) {
    if (str == "1") {
      return true;
    } else if (str == "0") {
      return false;
    }
  }
  function deleteUser() {
    api
      .delete(`/patients/${userID}/exams/${exameID}`)
      .then((res) => {
        Alert.alert(
          'EXAME APAGADO',
          'O exame foi deletado com sucecsso.',
          [
            { text: 'OK', onPress: () => navigation.navigate('Home') }
          ],
          { cancelable: false }
        );
      })
      .catch((e) => console.log("Erro!", e));
  }


  function getDados(user, exam) {
        setPeso(pacient?.exams?.weight?.toString());
        setAltura(pacient?.exams?.height?.toString());
        setSisDirUm(pacient?.exams?.sistolic_right_one?.toString());
        setSisEsqUm(pacient?.exams?.sistolic_left_one?.toString());
        setSisDirDois(pacient?.exams?.sistolic_right_two?.toString());
        setSisEsqDois(pacient?.exams?.sistolic_left_two?.toString());
        setDiaDirUm(pacient?.exams?.diastolic_right_one?.toString());
        setDiaEsqUm(pacient?.exams?.diastolic_left_one?.toString());
        setDiaDirDois(pacient?.exams?.diastolic_right_two?.toString());
        setDiaEsqDois(pacient?.exams?.diastolic_left_two?.toString());
        setHipertenso(convert(pacient?.exams?.have_hypertension?.toString()));
        setIg(pacient?.exams?.ig?.toString())
        setEclampPrevia(
          convert(pacient?.exams?.historic_eclampsy_previous?.toString())
        );
        setEclampFamilia(
          convert(pacient?.exams?.historic_family_eclampsy?.toString())
        );
        setNulipara(convert(pacient?.exams?.nulipara?.toString()));
  }

  const customStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 1,
    stepStrokeCurrentColor: "#0770b0",
    stepStrokeWidth: 1,
    stepStrokeFinishedColor: "#0770b0",
    stepStrokeUnFinishedColor: "#979797",
    separatorFinishedColor: "#0770b0",
    separatorUnFinishedColor: "#0770b0",
    stepIndicatorFinishedColor: "#0770b0",
    stepIndicatorUnFinishedColor: "#979797",
    stepIndicatorCurrentColor: "#0770b0",
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: "#FFF",
    stepIndicatorLabelFinishedColor: "#FFF",
    stepIndicatorLabelUnFinishedColor: "#FFF",
    labelColor: "#FFF",
    labelSize: 20,
    currentStepLabelColor: "#FFF",
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <Modal
        animationType="slide"
        transparent={true}
        visible={confirmDel}
        onRequestClose={() => setWrongUser(false)}
      >
        <TouchableOpacity
          style={{
            borderRadius: 25,
            height: "100%",
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(150, 30, 15, .4)",
          }}
          onPress={() => setConfirmDel(false)}
        >
          <View
            style={{
              backgroundColor: "rgba(255, 255, 255, .9)",
              width: "80%",
              height: "30%",
              borderRadius: 25,
              justifyContent: "center",
              alignItems: "center",
              paddingHorizontal: "10%",
              borderColor: "red",
              borderWidth: 2,
            }}
          >
            <FontAwesome name="warning" size={50} color="red" />
            <Text
              style={{
                fontSize: 20,
                fontWeight: "bold",
                color: "#076FAE",
                paddingVertical: "5%",
                textAlign: "center",
              }}
            >
              {" "}
              Tem certeza que deseja deletar o exame?
            </Text>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
                width: "100%",
              }}
            >
              <TouchableWithoutFeedback
                style={{ marginTop: "5%" }}
                onPress={() => deleteUser()}
              >
                <Text
                  style={{ fontSize: 20, fontWeight: "bold", color: "red" }}
                >
                  Sim
                </Text>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
                style={{ marginTop: "5%" }}
                onPress={() => setConfirmDel(false)}
              >
                <Text
                  style={{ fontSize: 20, fontWeight: "bold", color: "#076FAE" }}
                >
                  Não
                </Text>
              </TouchableWithoutFeedback>
            </View>
          </View>
        </TouchableOpacity>
      </Modal>
      <View style={styles.container}>
        <StatusBar backgroundColor="#043857" style="light" />
        <LinearGradient
          colors={["#076FAE", "#043857"]}
          start={[0.0, 0.0]}
          end={[0.3, 0.3]}
          style={styles.container}
        >
          <TopRow />
          <ScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            style={{ width: "100%" }}
          >
            <View style={styles.homeBody}>
              <Text style={styles.titleCenter}> EDITAR EXAME</Text>
              <View style={{ width: "80%" }}></View>
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>
                  Idade Gestacional (Semanas)
                </Text>
                <TextInput
                  keyboardType="numeric"
                  value={ig}
                  onChangeText={setIg}
                  placeholderTextColor="#707070"
                  placeholder="IG"
                />
              </View>
              <Text style={styles.title}>DADOS PARA IMC</Text>
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Peso (Kg)</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Peso"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={peso}
                    onChangeText={setPeso}
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Altura (m)</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Altura"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={altura}
                    onChangeText={setAltura}
                  ></TextInput>
                </View>
              </View>
              {tentou && (peso == "" || altura == ",") ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campos obrigatórios.
                </Text>
              ) : null}
              <Text
                style={{
                  color: "#646262",
                  fontSize: 16,
                  marginTop: "5%",
                  alignSelf: "flex-start",
                  marginLeft: "2.5%",
                }}
              >
                MEDIDA 1
              </Text>
              <Text style={styles.titleWarning}>
                Inclua as medidas dos braços direito e esquerdo
              </Text>
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Sistólico Esq.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Esquerdo"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={sisEsqUm}
                    onChangeText={setSisEsqUm}
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Diastólico Esq.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Esquerdo"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={diaEsqUm}
                    onChangeText={setDiaEsqUm}
                  ></TextInput>
                </View>
              </View>
              {tentou && (sisEsqUm && diaEsqUm) == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campos obrigatórios.
                </Text>
              ) : null}
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Sistólico Dir.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Direito"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={sisDirUm}
                    onChangeText={setSisDirUm}
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Diastólico Dir.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Direito"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={diaDirUm}
                    onChangeText={setDiaDirUm}
                  ></TextInput>
                </View>
              </View>
              {tentou && (sisDirUm && diaDirUm) == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campos obrigatórios.
                </Text>
              ) : null}
              <Text
                style={{
                  color: "#646262",
                  fontSize: 16,
                  marginTop: "7.5%",
                  alignSelf: "flex-start",
                  marginLeft: "2.5%",
                }}
              >
                MEDIDA 2
              </Text>
              <Text style={styles.titleWarning}>
                A medida 2 deve ser realizada 5 minutos após a medida 1
              </Text>
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Sistólico Esq.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Esquerdo"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={sisEsqDois}
                    onChangeText={setSisEsqDois}
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Diastólico Esq.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Esquerdo"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={diaEsqDois}
                    onChangeText={setDiaEsqDois}
                  ></TextInput>
                </View>
              </View>
              {tentou && (sisEsqDois && diaEsqDois) == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campos obrigatórios.
                </Text>
              ) : null}
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Sistólico Dir.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Direito"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={sisDirDois}
                    onChangeText={setSisDirDois}
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Diastólico Dir.</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Braço Direito"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={diaDirDois}
                    onChangeText={setDiaDirDois}
                  ></TextInput>
                </View>
              </View>
              {tentou && (sisDirDois && diaDirDois) == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campos obrigatórios.
                </Text>
              ) : null}
              <View style={styles.input100}>
                <Text style={styles.inputTitle}> Hipertenso</Text>
                <ModalSelector
                  data={[
                    { key: 0, label: 'Não', value: false },
                    { key: 1, label: 'Sim', value: true }
                  ]}
                  initValue={hipertenso ? 'Sim' : 'Não'}
                  style={{
                    borderWidth: 0,
                  }}
                  selectStyle={{
                    borderWidth: 0,
                  }}
                  selectTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  initValueTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  sectionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  optionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  onChange={(option) => { setHipertenso(option.value) }} />
                {/* <Picker
                  style={styles.pickerView}
                  itemStyle={styles.pickerItem}
                  selectedValue={hipertenso}
                  onValueChange={(itemValue, itemIndex) => {
                    setHipertenso(itemValue);
                  }}
                >
                  <Picker.Item label="Não" value={false} />
                  <Picker.Item label="Sim" value={true} />
                </Picker> */}
              </View>
              <View style={styles.input100}>
                <Text style={styles.inputTitle}> Nulípara</Text>
                <ModalSelector
                  data={[
                    { key: 0, label: 'Não', value: false },
                    { key: 1, label: 'Sim', value: true }
                  ]}
                  initValue={nulipara ? 'Sim' : 'Não'}
                  style={{
                    borderWidth: 0,
                  }}
                  selectStyle={{
                    borderWidth: 0,
                  }}
                  selectTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  initValueTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  sectionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  optionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  onChange={(option) => { setNulipara(option.value) }} />
                {/* <Picker
                  selectedValue={nulipara}
                  style={styles.pickerView}
                  itemStyle={styles.pickerItem}
                  onValueChange={(itemValue, itemIndex) => {
                    setNulipara(itemValue);
                  }}
                >
                  <Picker.Item label="Não" value={false} />
                  <Picker.Item label="Sim" value={true} />
                </Picker> */}
              </View>
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>
                  {" "}
                  Histórico prévio de eclâmpsia
                </Text>
                <ModalSelector
                  data={[
                    { key: 0, label: 'Não', value: false },
                    { key: 1, label: 'Sim', value: true }
                  ]}
                  initValue={eclampPrevia ? 'Sim' : 'Não'}
                  style={{
                    borderWidth: 0,
                  }}
                  selectStyle={{
                    borderWidth: 0,
                  }}
                  selectTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  initValueTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  sectionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  optionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  onChange={(option) => { setEclampPrevia(option.value) }} />
                {/* <Picker
                  selectedValue={eclampPrevia}
                  style={styles.pickerView}
                  itemStyle={styles.pickerItem}
                  onValueChange={(itemValue, itemIndex) => {
                    setEclampPrevia(itemValue);
                  }}
                >
                  <Picker.Item label="Não" value={false} />
                  <Picker.Item label="Sim" value={true} />
                </Picker> */}
              </View>
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>
                  {" "}
                  Histórico familiar de eclâmpsia
                </Text>
                <ModalSelector
                  data={[
                    { key: 0, label: 'Não', value: false },
                    { key: 1, label: 'Sim', value: true }
                  ]}
                  initValue={eclampFamilia ? 'Sim' : 'Não'}
                  style={{
                    borderWidth: 0,
                  }}
                  selectStyle={{
                    borderWidth: 0,
                  }}
                  selectTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  initValueTextStyle={{
                    color: '#707070',
                    textAlign: 'left',
                    fontSize: 14
                  }}
                  sectionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  optionTextStyle={{
                    color: '#707070',
                    fontSize: 14
                  }}
                  onChange={(option) => { setEclampFamilia(option.value) }} />

                {/* <Picker
                  selectedValue={eclampFamilia}
                  style={styles.pickerView}
                  itemStyle={styles.pickerItem}
                  onValueChange={(itemValue, itemIndex) => {
                    setEclampFamilia(itemValue);
                  }}
                >
                  <Picker.Item label="Não" value={false} />
                  <Picker.Item label="Sim" value={true} />
                </Picker> */}
              </View>
              <TouchableOpacity
                style={styles.nextButton}
                onPress={() => {
                  editUser()
                  setRefresherHome(!refresherHome);
                }}
              >
                <LinearGradient
                  colors={["#076FAE", "#0B4699"]}
                  start={[0.2, 0.0]}
                  end={[0.6, 1]}
                  style={styles.buttonContainer}
                >
                  <Text style={styles.buttonText}> Salvar</Text>
                </LinearGradient>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.nextButton}
                onPress={() => setConfirmDel(true)}
              >
                <LinearGradient
                  colors={["red", "darkred"]}
                  start={[0.2, 0.0]}
                  end={[0.6, 1]}
                  style={styles.buttonContainer}
                >
                  <Text style={styles.buttonText}> Apagar Exame</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </LinearGradient>
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    alignItems: "center",
  },
  topRow: {
    flexDirection: "row",
    height: "20%",
    width: "100%",
    paddingHorizontal: "5%",
    alignItems: "center",
  },
  profilePicture: {
    flex: 0.2,
    borderRadius: 100,
    height: "40%",
    resizeMode: "contain",
  },
  greetings: {
    flex: 0.7,
    color: "#FFF",
    fontSize: 25,
  },
  menuIcon: {
    flex: 0.1,
  },
  homeBody: {
    height: "100%",
    backgroundColor: "#FFF",
    width: "100%",
    borderTopLeftRadius: 55,
    alignItems: "center",
    paddingVertical: "10%",
    paddingHorizontal: "10%",
  },
  titleWarning: {
    color: "darkred",
    fontSize: 14,
    alignSelf: "flex-start",
    marginLeft: "2.5%",
  },
  titleCenter: {
    color: "#979797",
    fontSize: 20,
    alignSelf: "center",
    marginBottom: "5%",
  },
  title: {
    color: "#979797",
    fontSize: 20,
    marginTop: "10%",
    alignSelf: "flex-start",
  },
  input100: {
    width: "95%",
    marginTop: "5%",
    borderBottomWidth: 1,
    borderBottomColor: "#076FAE",
    paddingBottom: "2%",
  },
  inputTitle: {
    marginBottom: "2%",
    color: "#646262",
    fontSize: 16,
  },
  input50: {
    flex: 0.45,
    marginTop: "5%",
    borderBottomWidth: 1,
    borderBottomColor: "#076FAE",
    paddingBottom: "2%",
  },
  nextButton: {
    width: "80%",
    height: 50,
    marginTop: "5%",
  },
  buttonContainer: {
    borderRadius: 25,
    width: '100%',
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 20,
  },
  pickerView: {
    width: "100%",
    height: 20,
    color: "grey",
  },
  pickerItem: {
    height: Platform.OS === 'ios' ? 35 : 20,
    fontSize: 14,
  },
});
