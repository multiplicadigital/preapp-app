import React, { useState, useRef } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Button,
  TextInput,
  Picker,
  Alert,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import CheckBox from "@react-native-community/checkbox";
import { LinearGradient } from "expo-linear-gradient";

import api from "../../utils/api";

export default function novaSenhaScreen({ navigation }) {
  const [token, setToken] = useState("");
  const [password, setPassword] = useState("");

  const passwordRef = useRef();

  const handleRedefinirSenha = async () => {
    try {
      const res = await api.put("/forgotpassword/", {
        token,
        password,
      });
      navigation.navigate("Login");
      Alert.alert("Senha redefinida!");
    } catch (err) {}
  };

  return (
    <View style={styles.container}>
      <Image
        style={{ resizeMode: "contain", width: "35%", height: "30%" }}
        source={require("../../../assets/logo_nome.png")}
      />
      <View
        style={{
          borderLeftColor: "#00a0d2",
          borderLeftWidth: 4,
          backgroundColor: "#FFF",
          width: "80%",
          padding: "5%",
          marginBottom: "5%",
        }}
      >
        <Text style={{ color: "#707070", fontSize: 20 }}>
          Digite o token recebido por e-mail e sua nova senha.
        </Text>
      </View>
      <View style={styles.loginInput}>
        <Text style={styles.inputLabel}> Token</Text>
        <View style={styles.inputContainer}>
          <LinearGradient
            colors={["#076FAE", "#0B4699"]}
            start={[0.5, 0.5]}
            end={[1.0, 1.0]}
            style={styles.iconContainer}
          >
            <MaterialCommunityIcons
              name="account-question-outline"
              size={24}
              color="white"
            />
          </LinearGradient>
          <TextInput
            placeholder="Token"
            placeholderTextColor="#707070"
            style={styles.input}
            returnKeyType="next"
            onSubmitEditing={() => passwordRef.current.focus()}
            value={token}
            onChangeText={setToken}
          ></TextInput>
        </View>
      </View>
      <View style={styles.loginInput}>
        <Text style={styles.inputLabel}>Nova Senha</Text>
        <View style={styles.inputContainer}>
          <LinearGradient
            colors={["#076FAE", "#0B4699"]}
            start={[0.5, 0.5]}
            end={[1.0, 1.0]}
            style={styles.iconContainer}
          >
            <Image
              style={{ width: 25, height: 25 }}
              source={require("../../../assets/icon_lock.png")}
            />
          </LinearGradient>
          <TextInput
            autoCapitalize="none"
            secureTextEntry={true}
            placeholder="Nova Senha"
            placeholderTextColor="#707070"
            style={styles.input}
            returnKeyType="send"
            onSubmitEditing={handleRedefinirSenha}
            value={password}
            onChangeText={setPassword}
          ></TextInput>
        </View>
      </View>

      <TouchableOpacity style={styles.button} onPress={handleRedefinirSenha}>
        <LinearGradient
          colors={["#076FAE", "#0B4699"]}
          start={[0.2, 0.0]}
          end={[0.6, 1]}
          style={styles.buttonContainer}
        >
          <Text style={styles.buttonText}> Salvar nova senha</Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    paddingTop: "5%",
    alignItems: "center",
    marginTop: "10%",
    backgroundColor: "#f1f1f1",
  },
  button: {
    width: "100%",
    height: "100%",
    marginTop: "5%",
    marginBottom: "5%",
    alignItems: "center",
  },
  buttonContainer: {
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
    height: "7.5%",
    width: "60%",
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 17,
  },
  loginInput: {
    width: "100%",
    alignItems: "flex-start",
    paddingHorizontal: "10%",
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 17,
  },
  inputLabel: {
    color: "#707070",
    fontWeight: "100",
    fontSize: 15,
    marginVertical: "2%",
  },
  inputContainer: {
    flexDirection: "row",
    width: "100%",
    backgroundColor: "white",
    borderRadius: 5,
    height: 46,
    marginBottom: "2%",
  },
  iconContainer: {
    // flex: 0.17,
    justifyContent: "center",
    alignItems: "center",
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    width: "16%",
  },
  input: {
    overflow: "hidden",
    marginLeft: "2%",
    height: "100%",
    width: "100%",
  },
});
