import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Button,
  TextInput,
  Picker,
  Platform,
  KeyboardAvoidingView,
} from "react-native";
import { TextInputMask } from "react-native-masked-text";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons, Entypo } from "@expo/vector-icons";
import StepIndicator from "react-native-step-indicator";
import { ScrollView } from "react-native-gesture-handler";
import { StatusBar } from "expo-status-bar";
import { LinearGradient } from "expo-linear-gradient";
import { useCadastro } from "../../context/cadastro";
import AsyncStorage from '@react-native-async-storage/async-storage';
import TopRow from "../../Components/topRow/topRow";
import { roundToNearestMinutesWithOptions } from "date-fns/fp";
import { get } from "react-native/Libraries/Utilities/PixelRatio";

import ModalSelector from 'react-native-modal-selector'

let index = 0;
const data = [
  { label: "Estado", key: index++, section: true },
  { label: "AC", key: index++ },
  { label: "AL", key: index++ },
  { label: "AP", key: index++ },
  { label: "AM", key: index++ },
  { label: "BA", key: index++ },
  { label: "CE", key: index++ },
  { label: "ES", key: index++ },
  { label: "GO", key: index++ },
  { label: "MA", key: index++ },
  { label: "MT", key: index++ },
  { label: "MS", key: index++ },
  { label: "MG", key: index++ },
  { label: "PA", key: index++ },
  { label: "PB", key: index++ },
  { label: "PE", key: index++ },
  { label: "PI", key: index++ },
  { label: "RJ", key: index++ },
  { label: "RN", key: index++ },
  { label: "RS", key: index++ },
  { label: "RO", key: index++ },
  { label: "RR", key: index++ },
  { label: "SC", key: index++ },
  { label: "SP", key: index++ },
  { label: "SE", key: index++ },
  { label: "TO", key: index++ },
  { label: "DF", key: index++ },
];


export default function cadastroUmScreen({ route, navigation }) {
  const {
    nomePaciente,
    setNomePaciente,
    cep,
    setCep,
    endereco,
    setEndereco,
    complemento,
    setComplemento,
    numero,
    setNumero,
    bairro,
    setBairro,
    cidade,
    setCidade,
    estado,
    setEstado,
    telefone,
    setTelefone,
    setIbge
  } = useCadastro();
  const customStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 1,
    stepStrokeCurrentColor: "#0770b0",
    stepStrokeWidth: 1,
    stepStrokeFinishedColor: "#0770b0",
    stepStrokeUnFinishedColor: "#979797",
    separatorFinishedColor: "#0770b0",
    separatorUnFinishedColor: "#0770b0",
    stepIndicatorFinishedColor: "#0770b0",
    stepIndicatorUnFinishedColor: "#979797",
    stepIndicatorCurrentColor: "#0770b0",
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: "#FFF",
    stepIndicatorLabelFinishedColor: "#FFF",
    stepIndicatorLabelUnFinishedColor: "#FFF",
    labelColor: "#FFF",
    labelSize: 20,
    currentStepLabelColor: "#FFF",
  };
  const [user, setUser] = useState();
  const [tentou, setTentou] = useState(false);
  const [cepInv, setCepInv] = useState(false);
  const [cepFormat, setCepFormat] = useState('')
  const [ibgef, setIbgeF] = useState('')
  useEffect(() => {
    async function getUser() {
      const u = await AsyncStorage.getItem("@preapp:user");
      setUser(JSON.parse(u));
    }
    getUser();
  }, []);

  function getCep(num) {
    setCep(num);
    let cepFormatado = num.substr(0, 5) + num.substr(6, 4);
    if (num.length >= 9) {
      fetch(`http://viacep.com.br/ws/${cepFormatado}/json/`, {
        method: "GET",
      })
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.erro == true) {
            setCepInv(true);
          } else {
            setCepInv(false);
          }
          setCepFormat(cepFormatado)
          setCidade(responseJson.localidade);
          setEstado(responseJson.uf);
          setBairro(responseJson.bairro);
          setEndereco(responseJson.logradouro);
          setIbge(responseJson.ibge)
          setIbgeF(responseJson.ibge)
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }

  function testInputs() {
    setTentou(true);
    if (
      (nomePaciente &&
        cep &&
        endereco &&
        numero &&
        bairro &&
        cidade &&
        estado &&
        telefone) != ""
    ) {
      setTentou(false);
      navigation.navigate("CadastroDois");
    }
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <View style={styles.container}>
        <StatusBar backgroundColor="#043857" style="light" />
        <LinearGradient
          colors={["#076FAE", "#043857"]}
          start={[0.0, 0.0]}
          end={[0.3, 0.3]}
          style={styles.container}
        >
          <TopRow />
          <ScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            style={{ width: "100%" }}
          >
            <View style={styles.homeBody}>
              <Text style={styles.titleCenter}>CADASTRO</Text>
              <View style={{ width: "80%" }}>
                <StepIndicator
                  stepCount="3"
                  currentPosition="1"
                  customStyles={customStyles}
                />
              </View>
              <Text style={styles.title}>NOVO PACIENTE</Text>
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>Nome da Paciente</Text>
                <TextInput
                  value={nomePaciente}
                  onChangeText={setNomePaciente}
                  style={{ paddingLeft: 10 }}
                  placeholder="Digite o nome da paciente"
                  placeholderTextColor="#707070"
                ></TextInput>
              </View>
              {tentou && nomePaciente == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campo obrigatório.
                </Text>
              ) : null}
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>CEP</Text>
                <TextInputMask
                  type={"zip-code"}
                  value={cep}
                  onChangeText={(value) => getCep(value)}
                  style={{ paddingLeft: 10 }}
                  placeholder="CEP"
                  placeholderTextColor="#707070"
                  keyboardType="number-pad"
                />
              </View>
              {tentou && cep == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campo obrigatório.
                </Text>
              ) : null}
              {cepInv ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  CEP inválido! Verifique seu CEP.
                </Text>
              ) : null}
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>Endereço</Text>
                <TextInput
                  value={endereco}
                  onChangeText={setEndereco}
                  style={{ paddingLeft: 10 }}
                  placeholder="Endereço"
                  placeholderTextColor="#707070"
                ></TextInput>
              </View>
              {tentou && endereco == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campo obrigatório.
                </Text>
              ) : null}
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>IBGE</Text>
                <Text>{ibgef}</Text>
              </View>
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Complemento</Text>
                  <TextInput
                    value={complemento}
                    onChangeText={setComplemento}
                    style={{ paddingLeft: 10 }}
                    placeholder="Complemento"
                    placeholderTextColor="#707070"
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Número</Text>
                  <TextInput
                    value={numero}
                    onChangeText={setNumero}
                    style={{ paddingLeft: 10 }}
                    placeholder="Número"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                  ></TextInput>
                </View>
              </View>
              {tentou && (numero == "") ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campo obrigatório.
                </Text>
              ) : null}
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>Bairro</Text>
                <TextInput
                  value={bairro}
                  onChangeText={setBairro}
                  style={{ paddingLeft: 10 }}
                  placeholder="Bairro"
                  placeholderTextColor="#707070"
                ></TextInput>
              </View>
              {tentou && bairro == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campo obrigatório.
                </Text>
              ) : null}
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Cidade</Text>
                  <TextInput
                    value={cidade}
                    onChangeText={setCidade}
                    style={{ paddingLeft: 10 }}
                    placeholder="Cidade"
                    placeholderTextColor="#707070"
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Estado</Text>
                  <ModalSelector
                    data={data}
                    initValue={estado || "Estado"}
                    style={{
                      borderWidth: 0,
                    }}
                    selectStyle={{
                      borderWidth: 0,
                    }}
                    selectTextStyle={{
                      color: '#000',
                      fontSize: 14
                    }}
                    initValueTextStyle={{
                      color: estado === '' ? '#707070' : '#000',
                      fontSize: 14
                    }}
                    sectionTextStyle={{
                      color: '#000',
                      fontSize: 14
                    }}
                    optionTextStyle={{
                      color: '#000',
                      fontSize: 14
                    }}
                    onChange={(option) => { setEstado(option.label) }} />

                  {/* <Picker
                    selectedValue={estado}
                    style={styles.pickerView}
                    itemStyle={styles.pickerItem}
                    onValueChange={(itemValue, itemIndex) => {
                      setEstado(itemValue);
                    }}
                  >
                    <Picker.Item label="AC" value="AC" />
                    <Picker.Item label="AL" value="AL" />
                    <Picker.Item label="AP" value="AP" />
                    <Picker.Item label="AM" value="AM" />
                    <Picker.Item label="BA" value="BA" />
                    <Picker.Item label="CE" value="CE" />
                    <Picker.Item label="ES" value="ES" />
                    <Picker.Item label="GO" value="GO" />
                    <Picker.Item label="MA" value="MA" />
                    <Picker.Item label="MT" value="MT" />
                    <Picker.Item label="MS" value="MS" />
                    <Picker.Item label="MG" value="MG" />
                    <Picker.Item label="PA" value="PA" />
                    <Picker.Item label="PB" value="PB" />
                    <Picker.Item label="PE" value="PE" />
                    <Picker.Item label="PI" value="PI" />
                    <Picker.Item label="RJ" value="RJ" />
                    <Picker.Item label="RN" value="RN" />
                    <Picker.Item label="RS" value="RS" />
                    <Picker.Item label="RO" value="RO" />
                    <Picker.Item label="RR" value="RR" />
                    <Picker.Item label="SC" value="SC" />
                    <Picker.Item label="SP" value="SP" />
                    <Picker.Item label="SE" value="SE" />
                    <Picker.Item label="TO" value="TO" />
                    <Picker.Item label="DF" value="DF" />
                  </Picker> */}
                </View>
              </View>
              {tentou && (estado || cidade) == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campos obrigatórios.
                </Text>
              ) : null}
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>Telefone</Text>
                <TextInputMask
                  type={"cel-phone"}
                  options={{
                    maskType: "BRL",
                    withDDD: true,
                    dddMask: "(99) ",
                  }}
                  value={telefone}
                  onChangeText={setTelefone}
                />
              </View>
              {tentou && telefone == "" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campo obrigatório.
                </Text>
              ) : null}
              <TouchableOpacity
                style={styles.nextButton}
                onPress={() => {
                  setCep(cepFormat)
                  testInputs()
                }}
              >
                <LinearGradient
                  colors={["#076FAE", "#0B4699"]}
                  start={[0.2, 0.0]}
                  end={[0.6, 1]}
                  style={styles.buttonContainer}
                >
                  <Text style={styles.buttonText}> Próximo</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </LinearGradient>
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    alignItems: "center",
  },
  topRow: {
    flexDirection: "row",
    height: "20%",
    width: "100%",
    paddingHorizontal: "5%",
    alignItems: "center",
  },
  profilePicture: {
    flex: 0.2,
    borderRadius: 100,
    height: "40%",
    resizeMode: "contain",
  },
  greetings: {
    flex: 0.7,
    color: "#FFF",
    fontSize: 25,
  },
  menuIcon: {
    flex: 0.1,
  },
  homeBody: {
    height: "100%",
    backgroundColor: "#FFF",
    width: "100%",
    borderTopLeftRadius: 55,
    alignItems: "center",
    paddingVertical: "10%",
    paddingHorizontal: "10%",
  },
  titleCenter: {
    color: "#979797",
    fontSize: 20,
    alignSelf: "center",
    marginBottom: "5%",
  },
  title: {
    color: "#979797",
    fontSize: 20,
    marginTop: "10%",
    alignSelf: "flex-start",
  },
  input100: {
    width: "95%",
    marginTop: "5%",
    borderBottomWidth: 1,
    borderBottomColor: "#076FAE",
    paddingBottom: "2%",
  },
  inputTitle: {
    marginBottom: "2%",
    color: "#646262",
    fontSize: 16,
  },
  input50: {
    flex: 0.45,
    marginTop: "5%",
    borderBottomWidth: 1,
    borderBottomColor: "#076FAE",
    paddingBottom: "2%",
  },
  nextButton: {
    width: "80%",
    height: 50,
    marginTop: "5%",
  },
  buttonContainer: {
    borderRadius: 25,
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 20,
  },
  pickerView: {
    width: "100%",
    height: 20,
    color: "grey",
  },
  pickerItem: {
    height: Platform.OS === 'ios' ? 35 : 20,
    fontSize: 14,
  },
});
