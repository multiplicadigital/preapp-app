import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Button,
  TextInput,
  Picker,
  Platform,
  KeyboardAvoidingView,
  ActivityIndicator,
  Modal,
  TouchableOpacityBase,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons, Entypo } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import CheckBox from "@react-native-community/checkbox";
import { LinearGradient } from "expo-linear-gradient";
import { useAuth } from "../../Components/context/Auth";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
export default function loginScreen({ navigation }) {
  const { SignIn, signed, loading, wrongUser, setWrongUser } = useAuth();
  const [email, setemail] = useState("");
  const [senha, setsenha] = useState("");
  const [modalVis, setModalVis] = useState("");
  const [tentou, setTentou] = useState(false);
  const [senhaVis, setSenhaVis] = useState(false)
  useEffect(() => {
    if (signed) {
      navigation.navigate("Home");
    }
  }, [signed]);

  async function logar() {
    setTentou(false)
    if (senha != "" && email != "") {
      await SignIn({
        email: email,
        password: senha,
      });
      setTentou(false);
      setModalVis(false)
      setWrongUser(false)
    } else {
      setTentou(true);
    }
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      {loading == true ? (
        <View
          style={{
            zIndex: 1,
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(1, 1, 1, .4)",
            justifyContent: "center",
            alignItems: "center",
            position: "absolute",
          }}
        >
          <ActivityIndicator size="large" color="#0B4699" animating={true} />
          <Text style={{ fontSize: 20, color: "#0B4699", fontWeight: "bold" }}>
            Carregando
          </Text>
        </View>
      ) : null}
      <Modal
        animationType="slide"
        transparent={true}
        visible={wrongUser && !loading}
        onRequestClose={() => setWrongUser(false)}
      >
        <TouchableOpacity
          style={{
            borderRadius: 25,
            height: "100%",
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(1, 1, 1, .5)",
          }}
          onPress={() => setWrongUser(false)}
        >
          <View
            style={{
              backgroundColor: "rgba(255, 255, 255, 1)",
              width: "80%",
              height: "30%",
              borderRadius: 25,
              justifyContent: "center",
              alignItems: "center",
              paddingHorizontal: "10%",
            }}
          >
            <Text
              style={{
                fontSize: 20,
                fontWeight: "bold",
                color: "#0B4699",
                paddingVertical: "5%",
              }}
            >
              {" "}
              Opa! Algo deu errado.
            </Text>
            <Text
              style={{
                fontSize: 17,
                color: "#868d91",
                fontWeight: "bold",
                textAlign: "justify",
              }}
            >
              {" "}
              Houve um problema para se conectar ao preApp. Tente novamente.
            </Text>
            <TouchableWithoutFeedback
              style={{ marginTop: "5%" }}
              onPress={() => setWrongUser(false)}
            >
              <Text
                style={{ fontSize: 20, fontWeight: "bold", color: "#0B4699" }}
              >
                OK
              </Text>
            </TouchableWithoutFeedback>
          </View>
        </TouchableOpacity>
      </Modal>
      <Image
        style={styles.logo}
        source={require("../../../assets/logo_nome.png")}
      />
      <View style={styles.loginInput}>
        <Text style={styles.loginText}>Login</Text>
        <Text style={styles.inputLabel}>Digite seu e-mail</Text>
        <View style={styles.inputContainer}>
          <LinearGradient
            colors={["#076FAE", "#0B4699"]}
            start={[0.5, 0.5]}
            end={[1.0, 1.0]}
            style={styles.iconContainer}
          >
            <MaterialCommunityIcons
              name="account-outline"
              size={24}
              color="white"
            />
          </LinearGradient>
          <TextInput
            placeholder="E-mail"
            keyboardType="email-address"
            autoCapitalize='none'
            placeholderTextColor="#707070"
            style={styles.input}
            value={email}
            onChangeText={(value) => setemail(value)}
          ></TextInput>
        </View>
        {tentou && email == "" ? (
          <Text style={{ color: "red", fontSize: 12 }}>
            {" "}
            É necessário um endereço de e-mail para fazer login.
          </Text>
        ) : null}
        <Text style={styles.inputLabel}>Digite sua senha</Text>
        <View style={styles.inputContainer}>
          <LinearGradient
            colors={["#076FAE", "#0B4699"]}
            start={[0.5, 0.5]}
            end={[1.0, 1.0]}
            style={styles.iconContainer}
          >
            <Image
              style={{ width: 25, height: 25 }}
              source={require("../../../assets/icon_lock.png")}
            />
          </LinearGradient>
          <TextInput
            autoCapitalize="none"
            placeholder="Senha"
            placeholderTextColor="#707070"
            secureTextEntry={! senhaVis}
            style={styles.input}
            value={senha}
            onChangeText={(value) => setsenha(value)}
          ></TextInput>
          <View style={{width:'100%', height:"100%", justifyContent:'flex-start', alignItems:"center", flexDirection:'row'}}>
         <Entypo name={senhaVis == false? "eye" : "eye-with-line"}  size={24} color="#0B4699"  onPress={() => setSenhaVis(!senhaVis)}/>
         </View>
        </View>
        {tentou == true && senha == "" ? (
          <Text style={{ color: "red", fontSize: 12 }}>
            {" "}
            É necessário uma senha para fazer login.
          </Text>
        ) : null}
      </View>
      <TouchableOpacity style={styles.button} onPress={() => logar()}>
        <LinearGradient
          colors={["#076FAE", "#0B4699"]}
          start={[0.5, 0.5]}
          end={[1.0, 1.0]}
          style={styles.buttonGradient}
        >
          <Text style={styles.buttonText}>Entrar</Text>
        </LinearGradient>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("Cadastro")}>
        <LinearGradient
          colors={["#076FAE", "#0B4699"]}
          start={[0.5, 0.5]}
          end={[1.0, 1.0]}
          style={styles.buttonGradient}
        >
          <Text style={styles.buttonText}>Cadastrar</Text>
        </LinearGradient>
      </TouchableOpacity>
      <Text
        style={styles.forgotText}
        onPress={() => navigation.navigate("EsqueceuSenha")}
      >
        {" "}
        Esqueceu sua senha?
      </Text>

    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    paddingTop: "5%",
    alignItems: "center",
    marginTop: "10%",
    backgroundColor: "#f1f1f1",
    width: "100%",
    height: "100%",
  },
  logo: {
    resizeMode: "contain",
    width: "30%",
    height: "30%",
  },
  loginInput: {
    width: "100%",
    alignItems: "flex-start",
    paddingHorizontal: "10%",
  },
  loginText: {
    fontSize: 23,
    fontWeight: "bold",
    color: "#707070",
  },
  inputLabel: {
    color: "#707070",
    fontWeight: "100",
    fontSize: 15,
    marginVertical: "2%",
  },
  inputContainer: {
    flexDirection: "row",
    width: "100%",
    backgroundColor: "white",
    borderRadius: 5,
    height: 46,
    marginBottom: "2%",
  },
  iconContainer: {
    // flex: 0.17,
    justifyContent: "center",
    alignItems: "center",
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    width: "16%",
  },
  input: {
    marginLeft: "2%",
    height: "100%",
    width: "70%",
  },
  button: {
    width: "100%",
    alignItems: "center",
    marginTop: 10,
    height: 50,
  },
  buttonGradient: {
    width: "55%",
    height: "100%",
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    color: "#fff",
    fontSize: 25,
  },
  forgotText: {
    marginTop: "3%",
    color: "#076FAE",
    fontSize: 17,
  },
});
