import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Button,
  TextInput,
  Picker,
  Platform,
  KeyboardAvoidingView,
} from "react-native";
import { TextInputMask } from "react-native-masked-text";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons, Entypo } from "@expo/vector-icons";
import StepIndicator from "react-native-step-indicator";
import { ScrollView } from "react-native-gesture-handler";
import { StatusBar } from "expo-status-bar";
import { LinearGradient } from "expo-linear-gradient";
import { useCadastro } from "../../context/cadastro";
import AsyncStorage from "@react-native-async-storage/async-storage";
import TopRow from "../../Components/topRow/topRow";
import { differenceInWeeks, differenceInYears } from "date-fns";
export default function cadastroDoisScreen({ route, navigation }) {
  const {
    dataNascimento,
    setDataNasciemnto,
    peso,
    setPeso,
    altura,
    setAltura,
    dum,
    setDum,
    weeks,
    days,
    setWeeks,
    setDays,
  } = useCadastro();

  function formatDate() {
    var nascFormat =
      dataNascimento.substr(6, 9) +
      "-" +
      dataNascimento.substr(3, 2) +
      "-" +
      dataNascimento.substr(0, 2);
    var dumFormat =
      dum.substr(6, 9) + "-" + dum.substr(3, 2) + "-" + dum.substr(0, 2);
    var alturaFormat = String(Number(altura) / 100);
    if (peso.substr(3, 1) == "," || peso.substr(3, 1) == ".") {
      var pesoFormat = peso.substr(0, 3) + "." + peso.substr(4, 3);
    } else {
      var pesoFormat = peso.substr(0, 2) + "." + peso.substr(3, 3);
    }
    setDataNasciemnto(nascFormat);
    setDum(dumFormat);
    setAltura(alturaFormat);
    setPeso(pesoFormat);
    testInputs();
  }

  function calcAge() {
    var dataCalc =
      dataNascimento.substr(6, 9) +
      "-" +
      dataNascimento.substr(3, 2) +
      "-" +
      dataNascimento.substr(0, 2);
    var result = differenceInYears(new Date(), new Date(dataCalc));
    if (dataCalc.length == 10) return result;
  }

  function calcIMC() {
    let pesoUnf = "";
    if (peso.substr(3, 1) == "," || peso.substr(3, 1) == ".") {
      pesoUnf = peso.substr(0, 3) + "." + peso.substr(4, 3);
    } else {
      pesoUnf = peso.substr(0, 2) + "." + peso.substr(3, 3);
    }
    let alturaUnf = altura.substr(0, 1) + "." + altura.substr(2, 2);
    let alturaIMC = parseFloat(alturaUnf);
    let pesoIMC = parseFloat(pesoUnf);
    let IMC = (pesoIMC / (alturaIMC * alturaIMC)) * 10;
    return IMC;
  }

  const customStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 1,
    stepStrokeCurrentColor: "#0770b0",
    stepStrokeWidth: 1,
    stepStrokeFinishedColor: "#0770b0",
    stepStrokeUnFinishedColor: "#979797",
    separatorFinishedColor: "#0770b0",
    separatorUnFinishedColor: "#0770b0",
    stepIndicatorFinishedColor: "#0770b0",
    stepIndicatorUnFinishedColor: "#979797",
    stepIndicatorCurrentColor: "#0770b0",
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: "#FFF",
    stepIndicatorLabelFinishedColor: "#FFF",
    stepIndicatorLabelUnFinishedColor: "#FFF",
    labelColor: "#FFF",
    labelSize: 20,
    currentStepLabelColor: "#FFF",
  };

  const [user, setUser] = useState();
  const [tentou, setTentou] = useState(false);

  useEffect(() => {
    async function getUser() {
      const u = await AsyncStorage.getItem("@preapp:user");
      setUser(JSON.parse(u));
    }
    getUser();
  }, []);

  function testInputs() {
    setTentou(true);
    if (peso != "" && (dataNascimento && dum) != "--" && altura != ",") {
      setTentou(false);
      navigation.navigate("CadastroTres");
    }
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <View style={styles.container}>
        <StatusBar backgroundColor="#043857" style="light" />
        <LinearGradient
          colors={["#076FAE", "#043857"]}
          start={[0.0, 0.0]}
          end={[0.3, 0.3]}
          style={styles.container}
        >
          <TopRow />
          <ScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            style={{ width: "100%", flex: 1 }}
          >
            <View style={styles.homeBody}>
              <Text style={styles.titleCenter}>CADASTRO</Text>
              <View style={{ width: "80%" }}>
                <StepIndicator
                  stepCount="3"
                  currentPosition="2"
                  customStyles={customStyles}
                />
              </View>
              <Text style={styles.title}>DADOS OBSTÉTRICOS</Text>
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>Data de Nascimento</Text>
                <TextInputMask
                  type={"datetime"}
                  options={{
                    format: "DD/MM/AAAA",
                  }}
                  value={dataNascimento}
                  onChangeText={setDataNasciemnto}
                  placeholder="Data de nascimento"
                  placeholderTextColor="#707070"
                />
              </View>
              {tentou && dataNascimento == "--" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campo obrigatório.
                </Text>
              ) : null}
              <Text
                style={{ marginTop: "2%", color: "#646262", fontSize: 16, alignSelf: 'flex-start', marginLeft: 8 }}
              >
                Idade Gestacional
              </Text>
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <TextInput
                    style={{ fontSize: 16 }}
                    placeholder="Semanas"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={weeks}
                    onChangeText={setWeeks}
                  />
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                <TextInput
                    style={{ fontSize: 16 }}
                    placeholder="Dias"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={days}
                    onChangeText={setDays}
                  />
                </View>
              </View>
              {tentou && weeks == "--" ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campo obrigatório.
                </Text>
              ) : null}
              {weeks >= 43 ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Data inválida! Verifique se a data está correta.
                </Text>
              ) : null}
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>Idade</Text>
                <Text style={{ color: "#979797", paddingLeft: 10 }}>
                  {isNaN(calcAge()) ? "Idade" : calcAge() + " anos"}
                </Text>
              </View>
              <Text style={styles.title}>DADOS PARA IMC</Text>
              <View style={{ flexDirection: "row", width: "95%" }}>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Peso (Kg)</Text>
                  <TextInput
                    style={{ paddingLeft: 10 }}
                    placeholder="Peso"
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={peso}
                    onChangeText={setPeso}
                  ></TextInput>
                </View>
                <View style={{ flex: 0.1 }}></View>
                <View style={styles.input50}>
                  <Text style={styles.inputTitle}>Altura (m)</Text>
                  <TextInputMask
                    style={{ paddingLeft: 10, fontSize: 12 }}
                    type={"custom"}
                    options={{
                      mask: "9,99",
                    }}
                    placeholder="Ex: para 1,50m digite 150."
                    placeholderTextColor="#707070"
                    keyboardType="number-pad"
                    value={altura}
                    onChangeText={setAltura}
                  ></TextInputMask>
                </View>
              </View>
              {tentou && (peso == "" || altura == ",") ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Campos obrigatórios.
                </Text>
              ) : null}
              {parseFloat(altura.substr(0, 1) + "." + altura.substr(2, 2)) >
              2.5 ? (
                <Text
                  style={{
                    color: "red",
                    fontSize: 12,
                    alignSelf: "flex-start",
                    paddingLeft: 5,
                  }}
                >
                  {" "}
                  Altura Inválida.
                </Text>
              ) : null}
              <View style={styles.input100}>
                <Text style={styles.inputTitle}>IMC</Text>
                <Text style={{ color: "#979797", paddingLeft: 10 }}>
                  {isNaN(calcIMC()) ? "IMC" : calcIMC()}
                </Text>
              </View>
              <TouchableOpacity
                style={styles.nextButton}
                onPress={() => {
                  formatDate();
                }}
              >
                <LinearGradient
                  colors={["#076FAE", "#0B4699"]}
                  start={[0.2, 0.0]}
                  end={[0.6, 1]}
                  style={styles.buttonContainer}
                >
                  <Text style={styles.buttonText}> Próximo</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </LinearGradient>
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    alignItems: "center",
  },
  topRow: {
    flexDirection: "row",
    height: "20%",
    width: "100%",
    paddingHorizontal: "5%",
    alignItems: "center",
  },
  profilePicture: {
    flex: 0.2,
    borderRadius: 100,
    height: "40%",
    resizeMode: "contain",
  },
  greetings: {
    flex: 0.7,
    color: "#FFF",
    fontSize: 25,
  },
  menuIcon: {
    flex: 0.1,
  },
  homeBody: {
    height: "100%",
    backgroundColor: "#FFF",
    width: "100%",
    borderTopLeftRadius: 55,
    alignItems: "center",
    paddingVertical: "10%",
    paddingHorizontal: "10%",
  },
  titleCenter: {
    color: "#979797",
    fontSize: 20,
    alignSelf: "center",
    marginBottom: "5%",
  },
  title: {
    color: "#979797",
    fontSize: 20,
    marginTop: "10%",
    alignSelf: "flex-start",
  },
  input100: {
    width: "95%",
    marginTop: "5%",
    borderBottomWidth: 1,
    borderBottomColor: "#076FAE",
    paddingBottom: "2%",
  },
  inputTitle: {
    marginBottom: "2%",
    color: "#646262",
    fontSize: 16,
  },
  input50: {
    flex: 0.45,
    marginTop: "5%",
    borderBottomWidth: 1,
    borderBottomColor: "#076FAE",
    paddingBottom: "2%",
  },
  nextButton: {
    width: "80%",
    height: 50,
    marginTop: "5%",
  },
  buttonContainer: {
    borderRadius: 25,
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 20,
  },
});
