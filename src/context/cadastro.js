import React, { createContext, useState, useContext, useEffect } from "react";

const CadastroContext = createContext({});
export function CadastroProvider({ children }) {
  const [nomePaciente, setNomePaciente] = useState("");
  const [cep, setCep] = useState("");
  const [endereco, setEndereco] = useState("");
  const [complemento, setComplemento] = useState("");
  const [numero, setNumero] = useState("");
  const [bairro, setBairro] = useState("");
  const [cidade, setCidade] = useState("");
  const [estado, setEstado] = useState("");
  const [telefone, setTelefone] = useState("");
  const [dataNascimento, setDataNasciemnto] = useState("")
  const [peso, setPeso] = useState("")
  const [altura, setAltura] = useState("")
  const [dum, setDum] = useState("")
  const [sisDirUm, setSisDirUm] = useState("")
  const [sisEsqUm, setSisEsqUm] = useState("")
  const [sisDirDois, setSisDirDois] = useState("")
  const [sisEsqDois, setSisEsqDois] = useState("")
  const [diaDirUm, setDiaDirUm] = useState("")
  const [diaEsqUm, setDiaEsqUm] = useState("")
  const [diaDirDois, setDiaDirDois] = useState("")
  const [diaEsqDois, setDiaEsqDois] = useState("")
  const [hipertenso, setHipertenso] = useState(false)
  const [eclampPrevia, setEclampPrevia] = useState(false)
  const [eclampFamilia, setEclampFamilia] = useState(false)
  const [nulipara, setNulipara] = useState(false)
  const [ig, setIg] = useState("")
  const [ibge, setIbge] = useState("")
  const [refresherHome,setRefresherHome] = useState(false)
  return (
    <CadastroContext.Provider
      value={{
        nomePaciente,
        setNomePaciente,
        cep,
        setCep,
        endereco,
        setEndereco,
        complemento,
        setComplemento,
        numero,
        setNumero,
        bairro,
        setBairro,
        cidade,
        setCidade,
        estado,
        setEstado,
        telefone,
        setTelefone,
        dataNascimento,
        setDataNasciemnto,
        peso,
        setPeso,
        altura,
        setAltura,
        dum,
        setDum,
        sisDirUm,
        setSisDirUm,
        sisEsqUm,
        setSisEsqUm,
        sisDirDois,
        setSisDirDois,
        sisEsqDois,
        setSisEsqDois,
        diaDirUm,
        setDiaDirUm,
        diaEsqUm,
        setDiaEsqUm,
        diaDirDois,
        setDiaDirDois,
        diaEsqDois,
        setDiaEsqDois,
        hipertenso,
        setHipertenso,
        eclampPrevia,
        setEclampPrevia,
        eclampFamilia,
        setEclampFamilia,
        nulipara,
        setNulipara,
        ig,
        setIg,
        ibge,
        setIbge,
        refresherHome,
        setRefresherHome
      }}
    >
      {children}
    </CadastroContext.Provider>
  );
}

export function useCadastro() {
  const context = useContext(CadastroContext);
  return context;
}
