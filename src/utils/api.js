import axios from 'axios'
import base_url from './baseUrl'
import AsyncStorage from '@react-native-async-storage/async-storage';

const token = async () => await AsyncStorage.getItem('token')
const api = axios.create({
    baseURL: base_url,
})
if (token) {
    api.defaults.headers.Authorization = `Token ${token}`
}

export default api;

