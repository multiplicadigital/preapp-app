import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import loginScreen from "../screens/login/login";
import esqueceuSenhaScreen from "../screens/esqueceusenha/esqueceusenha";
import novaSenhaScreen from "../screens/novasenha/novaSenha";
import cadastroScreen from "../screens/cadastro/cadastro";
const Stack = createStackNavigator();

export default function AuthRoutes() {
  return (
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen
        name="EsqueceuSenha"
        component={esqueceuSenhaScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Cadastro"
        component={cadastroScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={loginScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="NovaSenha"
        component={novaSenhaScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}
