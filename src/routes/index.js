import { AppLoading } from "expo";
import React from "react";
import { View } from "react-native";

import { useAuth } from "../Components/context/Auth";

import AuthRoutes from "./auth.routes";
import HomeRoutes from "./home.routes";

export default function Routes() {
  const { loading, signed } = useAuth();

  if (loading) {
    <AppLoading />
  }

  if (signed) {
    return <HomeRoutes />;
  } else {
    return <AuthRoutes />;
  }
}
