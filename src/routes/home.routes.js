import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";

import CadastroUmScreen from "../screens/cadastroum/cadastroUm";
import CadastroDoisScreen from "../screens/cadastrodois/cadastroDois";
import CadastroTresScreen from "../screens/cadastrotres/cadastroTres";

import homeScreen from "../screens/home/home";
import editCadastroScreen from "../screens/editCadastro/editCadastro";
import cadastroCompletoScreen from "../screens/cadastrocompleto/cadastroCompleto";
import cadastroFalhaScreen from "../screens/cadastrofalha/cadastroFalha";
import classificacaoScreen from "../screens/classificacao/classificacao";
import adicionarExameScreen from "../screens/adicionarexame/addExame"
import DrawerContent from "../Components/Drawer";
import listaExamesScreen from '../screens/listaExames/listaExames'
import exameScreen from '../screens/exame/exame'
import novaSenhaScreen from '../screens/novasenha/novaSenha'

const Stack = createDrawerNavigator();

export default function HomeRoutes() {
  return (
    <Stack.Navigator
      drawerPosition="left"
      drawerType="slide"
      initialRouteName="Home"
      drawerContent={(props) => <DrawerContent {...props}/>}
    >
      <Stack.Screen
        name="CadastroUm"
        component={CadastroUmScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="AddExame"
        component={adicionarExameScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="CadastroDois"
        component={CadastroDoisScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="CadastroTres"
        component={CadastroTresScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Home"
        component={homeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Edit"
        component={editCadastroScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="CadastroCompleto"
        component={cadastroCompletoScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="CadastroFalha"
        component={cadastroFalhaScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Classificacao"
        component={classificacaoScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ListaExames"
        component={listaExamesScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Exame"
        component={exameScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="NovaSenha"
        component={novaSenhaScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
    
  );
}
