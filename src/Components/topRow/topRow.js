import React, { useState, useEffect } from "react";
import { View, StyleSheet, Image } from "react-native";
import { Entypo } from "@expo/vector-icons";
import Text from "../../Components/TextRoboto";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation, useNavigationState, useRoute } from "@react-navigation/native";
import IconGoBack from "../IconGoBack";

export default function TopRow() {
  const navigation = useNavigation();
  const route = useRoute();
  const [user, setUser] = useState();

  useEffect(() => {
    async function getUser() {
      const u = await AsyncStorage.getItem("@preapp:user");
      setUser(JSON.parse(u));
    }
    getUser();
  }, []);
  return (

    <View style={styles.topRow} >
      {
        !['Home'].includes(route.name) && (
          <IconGoBack color='#FFFF' />
        )
      }
      <Image
        style={styles.profilePicture}
        source={require("../../../assets/medico.png")}
      ></Image>
      <Text style={styles.greetings}> Olá, {user ? user.name : "Dr"}</Text>
      <Entypo
        style={styles.menuIcon}
        name="menu"
        size={40}
        color="white"
        onPress={() => navigation.toggleDrawer()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  topRow: {
    flexDirection: "row",
    height: "15%",
    width: "100%",
    paddingHorizontal: "5%",
    alignItems: "center",
    marginTop: '2.5%',
  },
  profilePicture: {
    flex: 0.2,
    borderRadius: 100,
    height: "50%",
    resizeMode: "contain",
  },
  greetings: {
    flex: 0.7,
    color: "#FFF",
    fontSize: 25,
  },
  menuIcon: {
    flex: 0.1,
  },
});
