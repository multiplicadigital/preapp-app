import React, { createContext, useState, useContext, useEffect } from "react";
import api from "../../utils/api";
import AsyncStorage from '@react-native-async-storage/async-storage';

const INITIAL_CONTEXT = {
  signed: false,
  loading: true,
  SignIn: () => {},
  SignOut: () => {},
};

const AuthContext = createContext(INITIAL_CONTEXT);

export default function AuthProvider({ children }) {
  const [loading, setLoading] = useState(true);
  const [signed, setSigned] = useState(false);
  const [user, setUser] = useState();
  const [wrongUser, setWrongUser] = useState(false);

  useEffect(() => {

    async function loadStorage() {
      const storageToken = await AsyncStorage.getItem("token");
      if (storageToken) {

        api.defaults.headers.Authorization = `Bearer ${storageToken}`;
        setSigned(true);

        setLoading(false);
      }
      setLoading(false);
    }
    loadStorage();

  }, []);

  async function SignIn(data) {
    setLoading(true);
    api
      .post("/session", data)
      .then(async (res) => {

        await AsyncStorage.setItem("token", res.data.token.token);
        await AsyncStorage.setItem(
          "@preapp:user",
          JSON.stringify(res.data.user)
        );
        api.defaults.headers.Authorization = `Bearer ${res.data.token.token}`;
        setSigned(true);
        setUser(JSON.stringify(res.data.user));
        setLoading(true);
        setWrongUser(false)
        return true;
      })
      .catch((err) => {
        console.log("Erro!", err);
        setLoading(false);
        setWrongUser(true);
        return false;
      });
  }

  async function SignOut() {
    await AsyncStorage.removeItem("token");
    setSigned(false);
    setLoading(false)
  }

  return (
    <AuthContext.Provider
      value={{ signed: signed, loading, SignIn, SignOut, user, wrongUser, setWrongUser }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export function useAuth() {
  const context = useContext(AuthContext);
  return context;
}
