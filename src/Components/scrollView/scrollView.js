import React, { useState, useEffect } from "react";
import { View, StyleSheet, RefreshControl } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons, Entypo, AntDesign } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import ActionButton from "react-native-action-button";
import PacienteCard from "../../Components/pacienteCard/pacienteCard";
import Text from "../../Components/TextRoboto";
import { ScrollView } from "react-native-gesture-handler";
import api from "../../utils/api";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from "@react-navigation/native";

export default function AppMainView({ refresh, refreshFunc, content }) {
  return (
    <View style={styles.bodyContainer}>
      <ScrollView
        style={styles.appMainView}
        contentContainerStyle={styles.scroll}
        refreshControl={
          <RefreshControl
            colors={["#076FAE"]}
            tintColor={"#076FAE"}
            onRefresh={() => refreshFunc()}
            style={{ backgroundColor: "transparent" }}
            refreshing={refresh}
          />
        }
        horizontal={false}
      >
          {content}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  appMainView: {
      width:'100%',
  },
  scroll: {
    flexGrow: 2,
    paddingHorizontal: "10%",
    marginBottom: "10%",
    paddingBottom : 100,
  },
  bodyContainer: {
    flex: 1,
    backgroundColor: "#FFF",
    borderTopLeftRadius: 55,
    width:'100%',
  },
});
