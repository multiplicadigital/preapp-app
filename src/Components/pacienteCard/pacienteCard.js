import React, { useEffect, useState } from "react";
import {
  Switch,
  ScrollView,
  StyleSheet,
  View,
  TouchableOpacity,
  StatusBar,
  Image,
  Alert,
  TouchableOpacityBase,
} from "react-native";
import Accordion from "react-native-collapsible/Accordion";
import { useNavigation } from "@react-navigation/native";
import * as Animatable from "react-native-animatable";
import Collapsible from "react-native-collapsible";
import iconBaixoRisco from "../../../assets/iconGreen.png";
import iconAltoRisco from "../../../assets/iconRed.png";
import Text from "../TextRoboto";
import differenceInYears from "date-fns/differenceInYears";
import { FontAwesome } from "@expo/vector-icons";
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
  AccordionList,
} from "accordion-collapse-react-native";
import { LinearGradient } from "expo-linear-gradient";
import { color } from "react-native-reanimated";
import { differenceInWeeks } from "date-fns";
import { render } from "react-dom";
import api from "../../utils/api";
import { useCadastro } from "../../context/cadastro";


const BACON_IPSUM =
  "Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs. Picanha beef prosciutto meatball turkey shoulder shank salami cupim doner jowl pork belly cow. Chicken shankle rump swine tail frankfurter meatloaf ground round flank ham hock tongue shank andouille boudin brisket. ";

const CONTENT = [
  {
    title: "teste",
    content: BACON_IPSUM,
  },
  {
    title: "Second",
    content: BACON_IPSUM,
  },
  {
    title: "Third",
    content: BACON_IPSUM,
  },
  {
    title: "Fourth",
    content: BACON_IPSUM,
  },
  {
    title: "Fifth",
    content: BACON_IPSUM,
  },
];

export default function PacienteCard({ paciente }) {
  const { setRefresherHome, refresherHome } = useCadastro();
  const navigation = useNavigation();
  const [activeSections, setActiveSections] = useState([]);
  const [IsFechado, setIsFechado] = useState(true);
  const [valida,setValida] = useState(false);
  toggleExpanded = () => {
    setCollapsed(!collapsed);
  };

  setSections = (sections) => {
    setActiveSections(sections.includes(undefined) ? [] : sections);
  };
  function valid(){
    if((paciente?.exams?true:false) == true){
      return setValida = false
    }else{
      return setValida = true
    }
  }
  function calcAge() {
    if(paciente?.born_date){
    var result = differenceInYears(
      new Date(),
      new Date(paciente?.born_date)
    );
    return result +" anos";
  }
    else{
      return " "
    }
  }

  function RenderHeader(section, _, isActive) {
    useEffect(() => {

    },[])
    if (IsFechado) {
 
      return (
        <View style={styles.header} transition="backgroundColor">
          <View
            style={{
              height: 50,
              width: "100%",
              // backgroundColor: "aqua",
              flexDirection: "row",
              // backgroundColor: "white",
              borderRadius: 10,
              // justifyContent: "center",
              alignItems: "center",
            }}
          >
            <View
              style={{
                flexDirection: "column",
                flex: 5,
                paddingLeft: 20,
              }}
            >
              <View style={{ flexDirection: "row" }}>
                {paciente.exams?<Image
                  style={{ marginRight: 8 }}
                  source={
                    paciente?.exams?.result?.type == "high"
                      ? iconAltoRisco
                      : iconBaixoRisco
                  }
                />:<Text></Text>}
                <Text style={{ fontSize: 15 }}>{paciente?.name}</Text>
              </View>
              {paciente.exams?<View style={{ flexDirection: "row", marginTop: "2%" }}>
                <Text style={{ fontSize: 13, marginRight: 20 }}>
                  {calcAge()}
                </Text>
                <Text style={{ fontSize: 13 }}>{paciente?.exams?.ig?paciente?.exams?.ig+" semanas":""}</Text>
              </View>:<Text style={{color:"green"}}>Paciente sem exames registrados</Text>}
            </View>
            {paciente.exams?<Text
              style={{
                color: paciente?.exams?.result?.type == "high" ? "red" : "green",
                flex: 1,
                marginRight: 15
              }}
            >
              {paciente?.exams?.result?.type == "high" ? "Alto" : "Baixo"}
            </Text>:<Text></Text>}
          </View>
          <View
            style={{
              width: "90%",
              backgroundColor: "rgba(0, 0, 0, 0.1)",
              height: 1,
              alignSelf: "center",
            }}
          />
        </View>
      );
    } else {
      return (
        <View style={styles.header}>
          <View
            style={{
              height: 200,
              width: "100%",
              // backgroundColor: "aqua",
              flexDirection: "row",
              // backgroundColor: "aqua",
              borderRadius: 10,
              // justifyContent: "center",
              alignItems: "center",
            }}
          >
            <View
              style={{
                flexDirection: "column",
                flex: 5,
                paddingLeft: 20
              }}
            >
             {paciente.exams?<View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  width: "100%",
                  paddingTop:20
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    // backgroundColor: "#ffa500",
                    flex: 8,
                  }}
                >
                  <Image
                    style={{ marginRight: 8 }}
                    source={
                      paciente?.exams?.result?.type == "high"
                        ? iconAltoRisco
                        : iconBaixoRisco
                    }
                  />
                  <Text style={{ fontSize: 15 }}>Classificação de Risco</Text>
                </View>
                <Text
                  style={{
                    color:
                      paciente?.exams?.result?.type == "high" ? "red" : "green",
                    flex: 2,
                    width: 10,
                  }}
                >
                  {paciente?.exams?.result?.type == "high" ? "Alto" : "Baixo"}
                </Text>
              </View>:<Text></Text>}

              {paciente.exams?<View
                style={{
                  backgroundColor: "rgba(0, 0, 0, .1)",
                  height: 1,
                  width: "100%",
                  marginTop: 10,
                }}
              />:<Text style={{color:"green", fontSize:18}}>Paciente sem exames registrados</Text>}

              <View style={{ flexDirection: "row" }}>
                {/* <Image style={{ marginRight: 8 }} source={iconBaixoRisco} /> */}
                {paciente.exams?<Text style={{ fontSize: 15, marginTop: 15 }}>
                  {paciente?.name}
                </Text>:<Text style={{ fontSize: 18, marginTop: 15 }}>{paciente?.name}</Text>}
              </View>

              <View style={{ flexDirection: "row", marginTop: "2%" } }>
                <Text style={{ fontSize: 13, marginRight: 20 }}>
                  {calcAge()}
                </Text>
                <Text style={{ fontSize: 13 }}>{paciente?.exams?.ig?paciente?.exams?.ig+"semanas":""}</Text>
                <View
                  style={{
                    justifyContent: "flex-end",
                    flexDirection: "row",
                    width: "55%",
                    marginLeft:160,
                    position:"absolute"
                  }}
                >
                  {paciente.exams?<TouchableOpacity
                    style={styles.nextButton}
                    onPress={() =>
                      navigation.navigate("Classificacao", { pacient: paciente})
                    }
                  >
                    <LinearGradient
                      colors={["#076FAE", "#0B4699"]}
                      start={[0.2, 0.0]}
                      end={[0.6, 1]}
                      style={styles.nextButton}
                    >
                      <Text style={{ color: "white", fontSize: 13 }}>
                      Relatório
                      </Text>
                    </LinearGradient>
                  </TouchableOpacity>:<Text></Text>}
                </View>
              </View>

              <View
                style={{
                  width: "100%",
                  justifyContent: "space-between",
                  alignItems: "flex-end",
                  // backgroundColor: "#fafafa",
                }}
              >
                <View style={styles.content}>
                    <View style={{flexDirection:'row', width:'100%', justifyContent:'space-around', height:'100%'}}>
                      {paciente.exams?
                      <TouchableOpacity
                        onPress={() =>{
                          navigation.navigate("Edit", { id: paciente.id })
                        }
                      }
                      >
                      <Text style={{ color: "#0B4699", fontSize:15, paddingVertical:10, paddingRight: 110, textAlign:'center',}}>
                          EDITAR {"\n"}
                        </Text>
                      </TouchableOpacity>
                      :
                      <TouchableOpacity
                        onPress={() =>{
                          api
                          .delete(`/patients/${paciente.id}`)
                          .then((res) => {
                            Alert.alert(
                              'USUÁRIO APAGADO',
                              'O usuário foi apagado com sucecsso.',
                              [
                                { text: 'OK', onPress: () => { 

                                  navigation.navigate('Home')
                                 } }
                              ],
                              { cancelable: false }
                            );
                          })
                          .catch((e) => console.log("Erro!", e));
                          setRefresherHome(!refresherHome);
                        }
                      }
                      >
                      <Text style={{ color: "#0B4699", fontSize:15, paddingVertical:10, paddingRight: 110, textAlign:'center',}}>
                          EXCLUIR {"\n"}
                        </Text>
                      </TouchableOpacity>}
                      <TouchableOpacity  
                      onPress={() =>{
                        navigation.navigate("AddExame", { id: paciente.id })
                      }
                      }> 
                        <Text style={{ color: "#0B4699", marginRight: "2%", fontSize:15, paddingRight: 10, paddingVertical:11, textAlign:'center' }}>
                          ADICIONAR EXAME{"\n"}
                        </Text>
                      </TouchableOpacity>
                    </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      );
    }
  }

  return (
    <Collapse
      style={{ marginTop: 8 }}
      onToggle={() => {

        setIsFechado(!IsFechado);
      }}
    >
      <CollapseHeader>
        <RenderHeader />
      </CollapseHeader>
      <CollapseBody>{/* <RenderContent /> */}</CollapseBody>
    </Collapse>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    width: "100%",
    shadowRadius: 2,
    shadowColor: "#00213b",
    shadowOpacity: 0.3,
    shadowOffset: {
      height: 2,
      width: 2,
    },
    borderRadius: 20,
  },
  nextButton: {
    width: "75%",
    // paddingHorizontal: 20,
    marginRight: "15%",
    marginBottom: 8,
    height: 30,
    borderRadius: 15,
    justifyContent: "space-around",
    alignItems: "center",
  },
  title: {
    textAlign: "center",
    fontSize: 22,
    fontWeight: "300",
    marginBottom: 20,
  },
  header: {
    // backgroundColor: "aqua",
    paddingTop: 5,
    marginBottom: 10,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "rgba(0, 0, 0, .1)",
    // shadowRadius: 2,
    // shadowColor: "#00213b",
    // shadowOpacity: 0.3,
    // shadowOffset: {
    //   height: 2,
    //   width: 2,
    // },
  },
  content: {
    // backgroundColor: "#ffa500",
    marginBottom: 10,
    justifyContent: "space-around",
    alignItems: "flex-end",
    borderRadius: 20,
    flexDirection: "row",
    width: "100%",
    height: 50,
  },
  headerText: {
    textAlign: "center",
    fontSize: 16,
    fontWeight: "500",
  },
  active: {
    backgroundColor: "rgba(255,255,255,1)",
  },
  inactive: {
    backgroundColor: "rgba(245,252,255,1)",
  },
  selectors: {
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "center",
  },
  selector: {
    backgroundColor: "#F5FCFF",
    padding: 10,
  },
  activeSelector: {
    fontWeight: "bold",
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: "500",
    padding: 10,
  },
  multipleToggle: {
    flexDirection: "row",
    justifyContent: "center",
    marginVertical: 30,
    alignItems: "center",
  },
  multipleToggle__title: {
    fontSize: 16,
    marginRight: 8,
  },
});
