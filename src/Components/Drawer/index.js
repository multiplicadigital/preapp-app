import React, { useState, useEffect } from "react";
import { View, Button, Image, Alert, Pressable } from "react-native";
import { color } from "react-native-reanimated";
import { LinearGradient } from "expo-linear-gradient";
import { TouchableOpacity } from "react-native-gesture-handler";
import Text from "../TextRoboto";
import { useAuth } from "../context/Auth";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { useNavigation } from "@react-navigation/native";
import axios from 'axios';
import * as Linking from 'expo-linking';
import base_url from '../../utils/baseUrl';
import api from "../../utils/api";
import { bool } from "yup";
import { boolean } from "yup";



function Drawer({ navigation }) {
  const [buttonDisabled, setbuttonDisabled] = useState(false)
  const [user, setUser] = useState();
  const { SignOut } = useAuth();
  const token = async () => await AsyncStorage.getItem('token')
  
  useEffect(() => {
    async function getUser() {
      const u = await AsyncStorage.getItem("@preapp:user");
      setUser(JSON.parse(u));
    }
    getUser();
  }, []);

  return (
    // <View style={{ height: 300, width: "100%", paddingTop: 50 }}>
    <LinearGradient
      colors={["#076FAE", "#043857"]}
      start={[0.0, 0.0]}
      end={[0.3, 0.3]}
      style={{ height: "100%", width: "100%", paddingTop: 50 }}
    >
      <View
        style={{
          width: "100%",
          alignItems: "center",
          height: "20%",
          justifyContent: "center",
          padding: 10,
        }}
      >
        {/* <View
          style={{
            backgroundColor: "white",
            width: "90%",
            height: 1,
            marginTop: 10,
            marginBottom: 10,
            alignSelf: "center",
          }}
        /> */}
        <Image
          source={require("../../../assets/medico.png")}
          style={{ height: 60, width: 60, borderRadius: 30 }}
        />
        <Text
          style={{
            color: "white",
            fontSize: 20,
            textAlign: "center",
          }}
        >
          {" "}
          Olá, {user ? user.name : "Dr"}
        </Text>
      </View>
      <View
        style={{
          backgroundColor: "white",
          width: "90%",
          height: 1,
          marginTop: 10,
          alignSelf: "center",
          alignItems:'center',
          justifyContent:'center'
        }}
      />
      <View
        style={{
          height: "77%",
          paddingTop:50,
          justifyContent:'space-between',
          alignItems:'center',
          width:'100%',
        }}
      >
        <View style={{width:'100%',alignItems:'center'}}>
        <TouchableOpacity
          style={{
            backgroundColor: "white",
            width: 220,
            justifyContent: "center",
            alignItems: "center",
            height: 35,
            borderRadius:5
            // position: "absolute",
            // top: 0,
          }}
          onPress={() => navigation.navigate('Home')}
        >
          <Text
            style={{
              color: "#076FAE",
              fontSize: 20,
              textAlign: "center",
              fontWeight: "bold",
            }}
          >
            Home
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            backgroundColor: "white",
            width: 220,
            justifyContent: "center",
            alignItems: "center",
            height: 35,
            marginTop: 15,
            borderRadius:5
            // position: "absolute",
            // top: 0,
          }}
          onPress={async () => {
            navigation.navigate('CadastroUm');
          }}
        >
          <Text
            style={{
              color: "#076FAE",
              fontSize: 20,
              textAlign: "center",
              fontWeight: "bold",
            }}
          >
            Cadastro
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            backgroundColor: "white",
            width: 220,
            justifyContent: "center",
            alignItems: "center",
            height: 35,
            marginTop: 15,
            borderRadius:5
            // position: "absolute",
            // top: 0,
          }}
          onPress={async () => {
            Linking.openURL('http://www.preapp.com.br');
          }}
        >
          <Text
            style={{
              color: "#076FAE",
              fontSize: 20,
              textAlign: "center",
              fontWeight: "bold",
            }}
          >
            Quem Somos?
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            backgroundColor: buttonDisabled?'rgba(52, 52, 52, 0.8)':"white",
            width: 220,
            justifyContent: "center",
            alignItems: "center",
            height: 35,
            marginTop: 15,
            borderRadius:5
          }}
          onPress={
            async () => {
            try {
              setbuttonDisabled(true)
              await api.get(`/export`, {
                token: token
              })
              Alert.alert(
                "Sucesso!",
                "Os dados foram enviados para o seu email.",
                [
                  {
                    text: "Fechar",
                    style: "cancel"
                  },
                ]
              )

            } catch (err) {
              console.log("Erro verifique suas credenciais ")
            }
          }}
          disabled={buttonDisabled}
        >
          <Text
            style={{
              color: "#076FAE",
              fontSize: 20,
              textAlign: "center",
              fontWeight: "bold",
            }}
          >
            Exportar
          </Text>
        </TouchableOpacity>
        </View>
        <TouchableOpacity
        style={{
          backgroundColor: "white",
          width: 220,
          justifyContent: "center",
          alignItems: "center",
          height: 35,
          marginTop: 50,
          borderRadius:5,
          bottom:50
           //position: "absolute",
           //bottom: 0,
        }}
        onPress={async () => {
          SignOut();
        }}
      >
        <Text
          style={{
            color: "#076FAE",
            fontSize: 20,
            textAlign: "center",
            fontWeight: "bold",
          }}
        >
          Sair
        </Text>
      </TouchableOpacity>
      </View>
    </LinearGradient>
    // </View>
  );
}

export default Drawer;
