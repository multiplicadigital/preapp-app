import React from "react";
import { Text } from "react-native";
import { useFonts, Roboto_400Regular } from "@expo-google-fonts/roboto";

export default function TextRoboto({ children, style }) {
  let estilo = style;
  let [fontsLoaded] = useFonts({
    Roboto_400Regular,
  });

  if (!fontsLoaded) {
    return <Text>{children}</Text>;
  }

  return (
    <Text style={{ fontFamily: "Roboto_400Regular", ...estilo }}>
      {children}
    </Text>
  );
}
