import React from 'react';
import { TouchableOpacity } from 'react-native';
import { FontAwesome } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/core';

// import { Container } from './styles';

const IconGoBack = ({ color, ...props }) => {
  const navigation = useNavigation()
  return (
    <TouchableOpacity onPress={() => navigation.goBack()} {...props} >
      <FontAwesome name="arrow-left" size={24} style={{ marginRight: 10 }} color={color || 'black'} />
    </TouchableOpacity>
  )
}

export default IconGoBack;