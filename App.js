import * as React from "react";
import { View, Text } from "react-native";
import { NavigationContainer } from "@react-navigation/native";

import AuthProvider from "./src//Components/context/Auth";
import { CadastroProvider } from "./src/context/cadastro";
import Routes from "./src/routes";
import DropDownPicker from 'react-native-dropdown-picker';
function App() {
  return (
    <CadastroProvider>
      <NavigationContainer>
        <AuthProvider>
          <Routes />
        </AuthProvider>
      </NavigationContainer>
    </CadastroProvider>
  );

}

export default App;
